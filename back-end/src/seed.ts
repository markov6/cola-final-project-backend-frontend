import 'reflect-metadata';
import { createConnection } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Administrator } from './data/entities/administrator';
import { Outlet } from './data/entities/outlet';
import { Customer } from './data/entities/customer';
import { User } from './data/entities/user';
import { Prize } from './data/entities/prizes';
import { WinningCode } from './data/entities/winning-codes';

const main = async () => {
  const connection = await createConnection();
  const adminRepo = connection.manager.getRepository(Administrator);
  const customerRepo = connection.manager.getRepository(Customer);
  const outletRepo = connection.manager.getRepository(Outlet);
  const userRepo = connection.manager.getRepository(User);
  const prizeRepo = connection.manager.getRepository(Prize);
  const winningCodesRepo = connection.manager.getRepository(WinningCode);

  const admin = await adminRepo.findOne({
    where: {
      username: 'admin',
    },
  });

  if (!admin) {
    const admin1 = new Administrator();
    (admin1.name = 'admin'),
      (admin1.email = 'admin@cola.com'),
      (admin1.password = await bcrypt.hash('P@ssw0rd!', 10));

    await adminRepo.save(admin1);
    // tslint:disable-next-line: no-console
    console.log(`admin created`);
  } else {
    // tslint:disable-next-line: no-console
    console.log(`admin already in the db`);
  }

  const customer1 = new Customer();
  customer1.name = 'Fantastico';
  await customerRepo.save(customer1);

  const customer2 = new Customer();
  customer2.name = 'Billa';
  await customerRepo.save(customer2);

  const customer3 = new Customer();
  customer3.name = 'Lidl';
  await customerRepo.save(customer3);

  const customer4 = new Customer();
  customer4.name = 'Kaufland';
  await customerRepo.save(customer4);

  const customer5 = new Customer();
  customer5.name = 'Metro';
  await customerRepo.save(customer5);

  const outlet2 = new Outlet();
  outlet2.name = 'Billa_Mladost 4';
  outlet2.brand = Promise.resolve(
    await customerRepo.findOne({ where: { name: 'Billa' } }),
  );
  await outletRepo.save(outlet2);

  const outlet3 = new Outlet();
  outlet3.name = 'Lidl_Mladost 4';
  outlet3.brand = Promise.resolve(
    await customerRepo.findOne({ where: { name: 'Lidl' } }),
  );
  await outletRepo.save(outlet3);

  const outlet4 = new Outlet();
  outlet4.name = 'Kaufland_Mladost 4';
  outlet4.brand = Promise.resolve(
    await customerRepo.findOne({ where: { name: 'Kaufland' } }),
  );
  await outletRepo.save(outlet4);

  const outlet5 = new Outlet();
  outlet5.name = 'Metro_Sofia';
  outlet5.brand = Promise.resolve(
    await customerRepo.findOne({ where: { name: 'Metro' } }),
  );
  await outletRepo.save(outlet5);

  const outlet1 = new Outlet();
  outlet1.name = 'Fantastico_Mladost 4';
  outlet1.brand = Promise.resolve(
    await customerRepo.findOne({ where: { name: 'Fantastico' } }),
  );
  await outletRepo.save(outlet1);

  /////////////////////////////
  const user1 = new User();
  user1.name = 'Ivan Ivanov';
  user1.email = 'ivanov@fantastico.com';
  user1.password = await bcrypt.hash('P@ssw0rd!', 10);
  user1.outlet = Promise.resolve(
    await outletRepo.findOne({ where: { name: 'Fantastico_Mladost 4' } }),
  );
  await userRepo.save(user1);

  const user2 = new User();
  user2.name = 'Petar Petrov';
  user2.email = 'petrov@billa.com';
  user2.password = await bcrypt.hash('P@ssw0rd!', 10);
  user2.outlet = Promise.resolve(
    await outletRepo.findOne({ where: { name: 'Billa_Mladost 4' } }),
  );
  await userRepo.save(user2);

  const user3 = new User();
  user3.name = 'Anton Antonov';
  user3.email = 'antonov@lidl.com';
  user3.password = await bcrypt.hash('P@ssw0rd!', 10);
  user3.outlet = Promise.resolve(
    await outletRepo.findOne({ where: { name: 'Lidl_Mladost 4' } }),
  );
  await userRepo.save(user3);

  const user4 = new User();
  user4.name = 'Georgi Georgiev';
  user4.email = 'georgiev@kaufland.com';
  user4.password = await bcrypt.hash('P@ssw0rd!', 10);
  user4.outlet = Promise.resolve(
    await outletRepo.findOne({ where: { name: 'Kaufland_Mladost 4' } }),
  );
  await userRepo.save(user4);

  const user5 = new User();
  user5.name = 'Doncho Donchev';
  user5.email = 'donchev@metro.com';
  user5.password = await bcrypt.hash('P@ssw0rd!', 10);
  user5.outlet = Promise.resolve(
    await outletRepo.findOne({ where: { name: 'Metro_Sofia' } }),
  );
  await userRepo.save(user5);

  // ////////////////////////////////
  const prize1 = new Prize();
  prize1.description = 'Soft Drink Can - 0.5 l';
  await prizeRepo.save(prize1);

  const prize2 = new Prize();
  prize2.description = 'Soft Drink Can - 0.333 l';
  await prizeRepo.save(prize2);

  const prize3 = new Prize();
  prize3.description = 'Soft Drink PET - 0.5 l';
  await prizeRepo.save(prize3);

  const prize4 = new Prize();
  prize4.description = 'Soft Drink PET - 1.5 l';
  await prizeRepo.save(prize4);

  const prize5 = new Prize();
  prize5.description = 'Soft Drink PET - 2 l';
  await prizeRepo.save(prize5);

  const prize6 = new Prize();
  prize6.description = 'Teddy bear';
  await prizeRepo.save(prize6);

  //////////////////////////////////

  const code1 = new WinningCode();
  code1.barcode = '1234567891118';
  code1.prize = Promise.resolve(
    await prizeRepo.findOne({ where: { description: 'Teddy bear' } }),
  );
  await winningCodesRepo.save(code1);

  const code2 = new WinningCode();
  code2.barcode = '2234567891117';
  code2.prize = Promise.resolve(
    await prizeRepo.findOne({ where: { description: 'Teddy bear' } }),
  );
  await winningCodesRepo.save(code2);

  const code3 = new WinningCode();
  code3.barcode = '3234567891116';
  code3.prize = Promise.resolve(
    await prizeRepo.findOne({
      where: { description: 'Soft Drink Can - 0.5 l' },
    }),
  );
  await winningCodesRepo.save(code3);

  const code4 = new WinningCode();
  code4.barcode = '4234567891115';
  code4.prize = Promise.resolve(
    await prizeRepo.findOne({
      where: { description: 'Soft Drink Can - 0.5 l' },
    }),
  );
  await winningCodesRepo.save(code4);

  const code5 = new WinningCode();
  code5.barcode = '5234567891114';
  code5.prize = Promise.resolve(
    await prizeRepo.findOne({
      where: { description: 'Soft Drink Can - 0.333 l' },
    }),
  );
  await winningCodesRepo.save(code5);

  const code6 = new WinningCode();
  code6.barcode = '6234567891113';
  code6.prize = Promise.resolve(
    await prizeRepo.findOne({
      where: { description: 'Soft Drink Can - 0.333 l' },
    }),
  );
  await winningCodesRepo.save(code6);

  const code7 = new WinningCode();
  code7.barcode = '7234567891112';
  code7.prize = Promise.resolve(
    await prizeRepo.findOne({
      where: { description: 'Soft Drink PET - 0.5 l' },
    }),
  );
  await winningCodesRepo.save(code7);

  const code8 = new WinningCode();
  code8.barcode = '8234567891111';
  code8.prize = Promise.resolve(
    await prizeRepo.findOne({
      where: { description: 'Soft Drink PET - 0.5 l' },
    }),
  );
  await winningCodesRepo.save(code8);

  const code9 = new WinningCode();
  code9.barcode = '9234567891110';
  code9.prize = Promise.resolve(
    await prizeRepo.findOne({
      where: { description: 'Soft Drink PET - 1.5 l' },
    }),
  );
  await winningCodesRepo.save(code9);

  connection.close();
};

// tslint:disable-next-line: no-console
main().catch(console.error);
