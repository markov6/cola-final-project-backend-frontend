import { BaseException } from '../base.exception';

export class EmailException extends BaseException {}
