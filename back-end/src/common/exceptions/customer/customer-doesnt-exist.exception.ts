import { BaseException } from '../base.exception';

export class CustomerDoesntExist extends BaseException {}
