import { BaseException } from '../base.exception';

export class OutletAlreadyExists extends BaseException {}
