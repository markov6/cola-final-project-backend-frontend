import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Administrator } from '../data/entities/administrator';
import { User } from '../data/entities/user';
import { CommonService } from './common.service';
import { Outlet } from '../data/entities/outlet';
import { Customer } from '../data/entities/customer';

@Module({
  imports: [TypeOrmModule.forFeature([User, Administrator, Outlet, Customer])],
  providers: [CommonService],
  exports: [CommonService],
})
export class CommonModule {}
