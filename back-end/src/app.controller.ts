import {
  Controller,
  Get,
  UseFilters,
  Post,
  HttpCode,
  UseGuards,
  HttpStatus,
  ValidationPipe,
  Body,
} from '@nestjs/common';
import { AppService } from './app.service';
import { UserExceptionFilter } from './common/filters/user-exception-filter';
import { AuthGuard } from '@nestjs/passport';
import { EmailDTO } from './models/email/email-dto';
import { SharedService } from './shared/shared/shared.service';

@Controller('api/')
@UseFilters(new UserExceptionFilter())
export class AppController {
  constructor(private readonly sharedService: SharedService) {}

  @Post('email')
  @HttpCode(HttpStatus.CREATED)
  public async sendEmail(
    @Body(new ValidationPipe({ transform: true, whitelist: true }))
    email: EmailDTO,
  ): Promise<any> {
    return this.sharedService.sendEmail(
      email.sender,
      email.subject,
      email.message,
    );
  }
}
