import {
  Controller,
  Post,
  Body,
  ValidationPipe,
  BadRequestException,
  UseFilters,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { User } from '../data/entities/user';
import { UsersService } from '../users/user.service';
import { UserLoginDTO } from '../models/user/user-login-dto';
import { UserRegisterDTO } from '../models/user/user-register-dto';
import { TokenDTO } from '../models/auth/token.dto';
import { UserExceptionFilter } from '../common/filters/user-exception-filter';
import { AuthGuard } from '@nestjs/passport';
import { AuthUser } from '../common/decorators/user.decorator';

@Controller('api/auth')
@UseFilters(new UserExceptionFilter())
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post()
  async login(
    @Body(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    )
    user: UserLoginDTO,
  ): Promise<TokenDTO> {
    return await this.authService.login(user);
  }

  @Post('admin')
  async loginAdmin(
    @Body(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    )
    user: UserLoginDTO,
  ): Promise<TokenDTO> {
    return await this.authService.loginAdmin(user);
  }

  @Delete()
  @UseGuards(AuthGuard())
  public async logOut(@AuthUser() user: any) {
    return await this.authService.logOut(user);
  }

  // @Post('register')
  // async register(@Body(new ValidationPipe({
  //   transform: true,
  //   whitelist: true,
  // })) user: UserRegisterDTO): Promise<User> {
  //   return await this.usersService.register(user);
  // }
}
