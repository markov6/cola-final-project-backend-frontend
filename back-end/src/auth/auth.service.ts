import {
  Injectable,
  HttpStatus,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from '../data/entities/user';
import { UsersService } from '../users/user.service';
import { UserLoginDTO } from '../models/user/user-login-dto';
import { JwtPayload } from '../models/auth/jwt.payload';
import { CommonService } from '../common/common.service';
import { UserNotFoundException } from '../common/exceptions/user/user-not-found.exception';
import { InvalidPasswordException } from '../common/exceptions/auth/invalid-password.exception';
import { Administrator } from '../data/entities/administrator';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
    private readonly commonService: CommonService,
  ) {}

  public async login(user: UserLoginDTO) {
    const checkExist = await this.commonService.findUserByEmail(user.email);

    if (!checkExist) {
      throw new UserNotFoundException(
        'User with such username doesnt exist!',
        HttpStatus.BAD_REQUEST,
      );
    }
    if (checkExist.isDeleted === true) {
      throw new UserNotFoundException(
        'Your User was deleted, contact Help Desk!',
        HttpStatus.BAD_REQUEST,
      );
    }

    if (!(await this.validateUserPass(user, checkExist))) {
      throw new InvalidPasswordException(
        'Invalid password!',
        HttpStatus.FORBIDDEN,
      );
    }

    const payload = {
      username: user.email,
      role: 'user',
    };

    const token = await this.jwtService.signAsync(payload);
    return {
      user: { username: user.email, userrole: 'user', id: checkExist.id },
      token,
    };
  }

  public async loginAdmin(user: UserLoginDTO) {
    const checkExist = await this.commonService.findAdminByEmail(user.email);

    if (!checkExist) {
      throw new UserNotFoundException(
        'User with such username doesnt exist!',
        HttpStatus.BAD_REQUEST,
      );
    }

    if (checkExist.isDeleted === true) {
      throw new BadRequestException(
        'Admin was deleted, you must create new one straight in the DB!',
      );
    }

    if (!(await this.validateUserPass(user, checkExist))) {
      throw new InvalidPasswordException(
        'Invalid password!',
        HttpStatus.FORBIDDEN,
      );
    }

    const payload = {
      username: user.email,
      role: 'admin',
    };

    const token = await this.jwtService.signAsync(payload);
    return {
      user: { username: user.email, userrole: 'admin', id: checkExist.id },
      token,
    };
  }

  public logOut(user: User | Administrator) {
    return 'logged out';
  }

  private async validateUserPass(
    user: UserLoginDTO,
    userEntity: User | Administrator,
  ): Promise<boolean> {
    return this.commonService.valiteUserPass(user, userEntity);
  }
}
