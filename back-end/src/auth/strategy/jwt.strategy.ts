import { Injectable, UnauthorizedException } from '@nestjs/common';
import { User } from '../../data/entities/user';
import { JwtPayload } from '../../models/auth/jwt.payload';
import { PassportStrategy } from '@nestjs/passport';
import { AuthService } from '../auth.service';
import { config } from '../../common/config';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { CommonService } from '../../common/common.service';
import { Administrator } from '../../data/entities/administrator';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
    private readonly commonService: CommonService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      // Change to the actual config service, don't use the hardcoded config object
      secretOrKey: config.jwtSecret,
    });
  }

  // async validate(payload: JwtPayload): Promise<User> {
  //   const user = await this.authService.validateUser({email: payload.email});
  //   if (!user) {
  //     throw new Error(`Not authorized!`);
  //   }

  //   return user;
  // }

  async validate(payload: any) {
    let user: any = {};
    if (payload.role === 'admin') {
      user = await this.commonService.findAdminByEmail(payload.username);
    }

    if (payload.role === 'user') {
      user = await this.commonService.findUserByEmail(payload.username);
    }

    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
