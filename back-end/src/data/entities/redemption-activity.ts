import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  VersionColumn,
  OneToMany,
  ManyToOne,
  Code,
} from 'typeorm';
import { User } from './user';
import { IsString, Matches } from 'class-validator';
import { userInfo } from 'os';
import { Customer } from './customer';
import { OuterSubscriber } from 'rxjs/internal/OuterSubscriber';
import { Outlet } from './outlet';
import { RedemptionStatus } from '../../models/redemption-code/redemptions-status-enum';

@Entity('redemption_activity')
export class RedemptionActivity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => User, user => user.redemptionActivities)
  user: Promise<User>;

  @UpdateDateColumn()
  timestamp: Date;

  @Column()
  code: string;

  @Column()
  outlet: string;

  @Column()
  customer: string;

  @Column({
    type: 'enum',
    enum: RedemptionStatus,
  })
  redemptionStatus: RedemptionStatus;

  @Column()
  prizeCode: string;
}
