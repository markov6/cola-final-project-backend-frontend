import {
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToOne,
  Entity,
} from 'typeorm';
import { User } from './user';
import { Administrator } from './administrator';

@Entity('admin_activity')
export class AdminActivity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  description: string;

  @CreateDateColumn()
  date: Date;

  @ManyToOne(type => Administrator, administrator => administrator.activities)
  admin: Promise<Administrator>;
}
