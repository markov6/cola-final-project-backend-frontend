import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  VersionColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { UserActivity } from './user-activity';
import { Outlet } from './outlet';
import { Customer } from './customer';
import { RedemptionActivity } from './redemption-activity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  name: string;

  @Column('nvarchar')
  password: string;

  @Column({ unique: true })
  email: string;

  @Column({ default: 'user' })
  priviliges: string;

  @Column({ default: false })
  isDeleted: boolean;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @VersionColumn()
  version: number;

  @OneToMany(type => RedemptionActivity, activity => activity.user)
  redemptionActivities: Promise<RedemptionActivity[]>;

  @OneToMany(type => UserActivity, activity => activity.user)
  activities: Promise<UserActivity[]>;

  @ManyToOne(type => Outlet, outlet => outlet.employees)
  outlet: Promise<Outlet>;

  @Column()
  phone: string;
}
