import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  AfterLoad,
} from 'typeorm';
import { Prize } from './prizes';

@Entity('winning_codes')
export class WinningCode {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar', { unique: true })
  barcode: string;

  @Column({ default: false })
  redeemed: boolean;

  @ManyToOne(type => Prize, prize => prize.winningCodes)
  prize: Promise<Prize>;
}
