import { MigrationInterface, QueryRunner } from 'typeorm';

// tslint:disable-next-line: class-name
export class userActivitiyAddingOutletAndBrand1563871181544
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      'ALTER TABLE `activities` ADD `outlet` varchar(255) NOT NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `activities` ADD `customer` varchar(255) NOT NULL',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query('ALTER TABLE `activities` DROP COLUMN `customer`');
    await queryRunner.query('ALTER TABLE `activities` DROP COLUMN `outlet`');
  }
}
