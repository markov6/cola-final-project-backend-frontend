import { Expose } from 'class-transformer';

export class ShowOutletDTO {
  @Expose()
  id: string;

  @Expose()
  name: string;

  @Expose()
  customer: string;

  @Expose()
  newBrand: string;
  // @Expose()
  // employees: string;
}
