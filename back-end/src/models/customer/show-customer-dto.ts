import { Expose } from 'class-transformer';

export class ShowCustomerDTO {
  @Expose()
  name: string;
}
