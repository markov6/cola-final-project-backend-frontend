import { IsString } from 'class-validator';

export class UpdateActivityWithPrizeDTO {
  @IsString()
  prizeBarcode: string;
}
