import { Expose } from 'class-transformer';

export class ShowRedemptionCodeActivityDTO {
  @Expose()
  id: string;
  @Expose()
  timestamp: string;
  @Expose()
  code: string;
  @Expose()
  status: boolean;
  @Expose()
  outlet: string;
  @Expose()
  customer: string;
  @Expose()
  redemptionStatus: string;
  @Expose()
  prizeCode: string;
  @Expose()
  username: string;
}
