import { Expose } from 'class-transformer';

export class RedemptionActivityDTO {
  @Expose()
  id: string;
}
