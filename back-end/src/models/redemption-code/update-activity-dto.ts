import { IsString } from 'class-validator';

export class UpdateActivityDTO {
  @IsString()
  id: string;
}
