import { Expose } from 'class-transformer';

export class UserActivityDTO {
  @Expose()
  id: string;
  @Expose()
  description: string;
  @Expose()
  date: Date;
  @Expose()
  customer: string;
  @Expose()
  outlet: string;
  @Expose()
  visibleUser: string;
}
