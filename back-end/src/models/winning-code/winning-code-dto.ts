import { Expose } from 'class-transformer';

export class WinningCodeDTO {
  @Expose()
  barcode: string;
  @Expose()
  redeemed: boolean;
  @Expose()
  prize: string;
}
