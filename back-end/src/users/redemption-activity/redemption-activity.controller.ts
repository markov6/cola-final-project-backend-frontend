import {
  Controller,
  Post,
  HttpCode,
  Put,
  Param,
  HttpStatus,
  UseGuards,
  Get,
  UseFilters,
  ValidationPipe,
  Body,
  Query,
} from '@nestjs/common';
import { RedemptionActivityService } from './redemption-activity.service';
import { AuthUser } from '../../common/decorators/user.decorator';
import { User } from '../../data/entities/user';
import { AuthGuard } from '@nestjs/passport';
import { WinningCode } from '../../data/entities/winning-codes';
import { WinningCodeDTO } from '../../models/winning-code/winning-code-dto';
import { UserExceptionFilter } from '../../common/filters/user-exception-filter';
import { CheckCodeDTO } from '../../models/redemption-code/check-code-dto';
import { RedemptionActivityDTO } from '../../models/redemption-code/redemption-activity-dto';
import { UpdateActivityDTO } from '../../models/redemption-code/update-activity-dto';
import { RedemptionDetailsDTO } from '../../models/redemption-code/redemption-detail';
import { UpdateActivityWithPrizeDTO } from '../../models/redemption-code/update-activity-with-prize-dto';
import { RolesGuard } from '../../common/guards/roles.guard';
import { ShowRedemptionCodeActivityDTO } from '../../models/redemption-code/show-redemption-code-activity';
import { Roles } from '../../common/decorators/roles.decorator';
import { ShowAllCodesDTO } from '../../models/winning-code/show-all-codes-dto';

@Controller('api/codes')
@UseFilters(new UserExceptionFilter())
export class RedemptionActivityController {
  constructor(
    private readonly RedemptionActivityServices: RedemptionActivityService,
  ) {}

  // @Put(':id')
  // @HttpCode(HttpStatus.ACCEPTED)
  // @UseGuards(AuthGuard())
  // public async getRedemptionCode(@Param('id') id: string, @AuthUser() user: User): Promise <RedemptionCode> {
  //   return await this.RedemptionActivityServices.getRedemptionCode(id, user);
  // }

  @Get('allwinningcodes')
  @Roles('admin')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard(), RolesGuard)
  public async getAllWinningCodes(): Promise<ShowAllCodesDTO[]> {
    return await this.RedemptionActivityServices.getAllWinningCodes();
  }

  @Get('redemptionactivities')
  // @Roles('admin')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard())
  public async getAllActivities(
    @Query('userId') userId: string,
    @Query('outlet') outlet: string,
  ): Promise<ShowRedemptionCodeActivityDTO[]> {
    return await this.RedemptionActivityServices.getAllRedemptionActivities(
      userId,
      outlet,
    );
  }

  @Get(':code')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard())
  public async checkCode(@Param('code') code: string): Promise<WinningCodeDTO> {
    return await this.RedemptionActivityServices.checkCode(code);
  }

  @Post('scanned/:code')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard())
  public async saveScan(
    @Body(new ValidationPipe({ transform: true, whitelist: true }))
    checkCode: CheckCodeDTO,
    @AuthUser() user: User,
  ): Promise<RedemptionActivityDTO> {
    return await this.RedemptionActivityServices.saveScan(checkCode, user);
  }

  @Put('scanned/:code')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard())
  public async updateActivity(
    @Param('code') code: string,
    @Body(new ValidationPipe({ transform: true, whitelist: true }))
    update: UpdateActivityDTO,
  ): Promise<RedemptionActivityDTO> {
    return await this.RedemptionActivityServices.updateActivityOnRedeem(
      update.id,
      code,
    );
  }

  @Get('scanned/:code')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard())
  public async getRedeemedCodeDetails(
    @Param('code') code: string,
  ): Promise<RedemptionDetailsDTO> {
    return await this.RedemptionActivityServices.getRedeemedCodeDetails(code);
  }

  @Put('activities/:id')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard())
  public async redeemWithPrize(
    @Param('id') activityId: string,
    @Body(new ValidationPipe({ transform: true, whitelist: true }))
    update: UpdateActivityWithPrizeDTO,
  ): Promise<RedemptionActivityDTO> {
    return await this.RedemptionActivityServices.redeemWithPrize(
      activityId,
      update.prizeBarcode,
    );
  }

  @Post('email/:code')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard())
  public async reportRedemptionAttempt(
    @Param('code') code: string,
    @AuthUser() user: User,
  ): Promise<{}> {
    return await this.RedemptionActivityServices.reportRedemptionAttempt(
      code,
      user.email,
      user.name,
    );
  }
}
