import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../data/entities/user';
import { Repository, getManager } from 'typeorm';
import { WinningCode } from '../../data/entities/winning-codes';
import { Prize } from '../../data/entities/prizes';
import { WinningCodeDTO } from '../../models/winning-code/winning-code-dto';
import { plainToClass } from 'class-transformer';
import { RedemptionActivity } from '../../data/entities/redemption-activity';
import { RedemptionStatus } from '../../models/redemption-code/redemptions-status-enum';
import { CheckCodeDTO } from '../../models/redemption-code/check-code-dto';
import { RedemptionActivityDTO } from '../../models/redemption-code/redemption-activity-dto';
import { RedemptionDetailsDTO } from '../../models/redemption-code/redemption-detail';
import { EmailException } from '../../common/exceptions/email/email-error';
import { SharedService } from '../../shared/shared/shared.service';
import { ShowRedemptionCodeActivityDTO } from '../../models/redemption-code/show-redemption-code-activity';
import { UserNotFoundException } from '../../common/exceptions/user/user-not-found.exception';
import { Outlet } from '../../data/entities/outlet';
import { OutletNotFoundException } from '../../common/exceptions/outlet/outlet-not-found.exception';
import { ShowAllCodesDTO } from '../../models/winning-code/show-all-codes-dto';

@Injectable()
export class RedemptionActivityService {
  constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    @InjectRepository(Outlet) private readonly outletRepo: Repository<Outlet>,
    @InjectRepository(WinningCode)
    private readonly winningCodeRepo: Repository<WinningCode>,
    @InjectRepository(Prize) private readonly prizeRepo: Repository<Prize>,
    @InjectRepository(RedemptionActivity)
    private readonly redemptionActivityRepo: Repository<RedemptionActivity>,
    private readonly sharedService: SharedService,
  ) {}

  public async getAllWinningCodes(): Promise<ShowAllCodesDTO[]> {
    const allWinningCodes = await this.winningCodeRepo.find();
    const codeWithPrize = await Promise.all(
      allWinningCodes.map(async code => {
        const prize = (await code.prize).description;
        const newCode = { ...code, prize };
        return newCode;
      }),
    );
    return plainToClass(ShowAllCodesDTO, codeWithPrize, {
      excludeExtraneousValues: true,
    });
  }

  public async checkCode(barcode: string): Promise<WinningCodeDTO> | undefined {
    const foundCode = await this.winningCodeRepo.findOne({ barcode });
    if (!!foundCode) {
      const prize = await foundCode.prize;
      return {
        barcode,
        prize: prize.description,
        redeemed: foundCode.redeemed,
      };
    } else {
      return { barcode, prize: `Not winning!`, redeemed: false };
    }
  }

  public async saveScan(
    checkCode: CheckCodeDTO,
    user: User,
  ): Promise<RedemptionActivityDTO> {
    const activity = this.redemptionActivityRepo.create();
    activity.code = checkCode.code;
    activity.user = Promise.resolve(user);
    const outlet = await user.outlet;
    activity.outlet = outlet.name;
    const customer = await outlet.brand;
    activity.customer = customer.name;

    if (checkCode.winning && !checkCode.redeemed) {
      activity.redemptionStatus = RedemptionStatus.CANCELLED;
    } else if (checkCode.winning && checkCode.redeemed) {
      activity.redemptionStatus = RedemptionStatus.REDEEMED;
    } else {
      activity.redemptionStatus = RedemptionStatus.INVALID;
    }

    const savedActivity = await this.redemptionActivityRepo.save(activity);
    return plainToClass(RedemptionActivityDTO, savedActivity, {
      excludeExtraneousValues: true,
    });
  }

  public async updateActivityOnRedeem(
    activityId: string,
    code: string,
  ): Promise<RedemptionActivityDTO> {
    const foundActivity = await this.redemptionActivityRepo.findOne(activityId);
    foundActivity.redemptionStatus = RedemptionStatus.SUCCESS;

    const foundBarcode = await this.winningCodeRepo.findOne({
      where: {
        barcode: code,
      },
    });

    foundBarcode.redeemed = true;

    const updatedActivity = await this.redemptionActivityRepo.save(
      foundActivity,
    );
    await this.winningCodeRepo.save(foundBarcode);
    return plainToClass(RedemptionActivityDTO, updatedActivity, {
      excludeExtraneousValues: true,
    });
  }

  public async getRedeemedCodeDetails(
    code: string,
  ): Promise<RedemptionDetailsDTO> {
    const foundActivity = await this.redemptionActivityRepo.findOne({
      where: {
        code,
        redemptionStatus: 'successful',
      },
    });

    const username = (await foundActivity.user).name;

    const redemptionDetails = plainToClass(
      RedemptionDetailsDTO,
      foundActivity,
      { excludeExtraneousValues: true },
    );
    redemptionDetails.username = username;
    return redemptionDetails;
  }

  public async redeemWithPrize(
    activityId: string,
    prizeBarcode: string,
  ): Promise<RedemptionActivityDTO> {
    const foundActivity = await this.redemptionActivityRepo.findOne(activityId);
    foundActivity.redemptionStatus = RedemptionStatus.SUCCESS;
    foundActivity.prizeCode = prizeBarcode;

    const winningBarcode = foundActivity.code;

    const foundBarcode = await this.winningCodeRepo.findOne({
      where: {
        barcode: winningBarcode,
      },
    });

    foundBarcode.redeemed = true;

    const updatedActivity = await this.redemptionActivityRepo.save(
      foundActivity,
    );
    await this.winningCodeRepo.save(foundBarcode);
    return plainToClass(RedemptionActivityDTO, updatedActivity, {
      excludeExtraneousValues: true,
    });
  }

  public async reportRedemptionAttempt(
    code: string,
    userEmail: string,
    userName: string,
  ): Promise<{}> {
    const content = `User ${userName} reports redemption attempt for already redeemed code ${code}`;
    const subject = 'Code Redemption Report';
    try {
      await this.sharedService.sendEmail(userEmail, subject, content);
    } catch (error) {
      return new EmailException('Something went wrong trying to send an email');
    }
  }

  public async getAllRedemptionActivities(
    userId: string,
    outlet: string,
  ): Promise<any> {
    if (!userId && !outlet) {
      const allActivities: RedemptionActivity[] = await this.redemptionActivityRepo.find();
      const userActivity = await Promise.all(
        allActivities.map(async activity => {
          const username = (await activity.user).name;
          const newActivity = { ...activity, username };
          return newActivity;
        }),
      );

      return plainToClass(ShowRedemptionCodeActivityDTO, userActivity, {
        excludeExtraneousValues: true,
      });
    }

    if (userId) {
      const foundUser: User = await this.userRepo.findOne({
        where: {
          id: userId,
        },
      });

      if (!foundUser) {
        throw new UserNotFoundException(`Couldn't find user with that Id`);
      }

      const allActivities: RedemptionActivity[] = await this.redemptionActivityRepo.find(
        {
          where: {
            user: foundUser,
          },
        },
      );

      const userActivity = allActivities.map(activity => {
        const username = foundUser.name;
        const newActivity = { ...activity, username };
        return newActivity;
      });

      return plainToClass(ShowRedemptionCodeActivityDTO, userActivity, {
        excludeExtraneousValues: true,
      });
    }

    if (outlet) {
      const foundOutlet: Outlet = await this.outletRepo.findOne({
        where: {
          name: outlet,
        },
      });

      if (!foundOutlet) {
        throw new OutletNotFoundException(
          `Couldn't find outlet with that name`,
        );
      }

      const allActivities: RedemptionActivity[] = await this.redemptionActivityRepo.find(
        {
          where: {
            outlet: foundOutlet.name,
          },
        },
      );

      const userActivity = await Promise.all(
        allActivities.map(async activity => {
          const username = (await activity.user).name;
          const newActivity = { ...activity, username };
          return newActivity;
        }),
      );

      return plainToClass(ShowRedemptionCodeActivityDTO, userActivity, {
        excludeExtraneousValues: true,
      });
    }
  }
}
