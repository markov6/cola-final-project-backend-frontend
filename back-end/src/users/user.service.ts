import { Injectable, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../data/entities/user';
import { UserRegisterDTO } from '../models/user/user-register-dto';
import { CommonService } from '../common/common.service';
import { UserAlreadyExistException } from '../common/exceptions/user/user-already-exists.exception';
import { OutletNotFoundException } from '../common/exceptions/outlet/outlet-not-found.exception';
import { ShowUserDTO } from '../models/user/show-user.dto';
import { plainToClass } from 'class-transformer';
import { CustomerRegisterDTO } from '../models/customer/customer-register-dto';
import { CustomerAlreadyExists } from '../common/exceptions/customer/customer-already-exists.exception';
import { Customer } from '../data/entities/customer';
import { Outlet } from '../data/entities/outlet';
import { ShowCustomerDTO } from '../models/customer/show-customer-dto';
import { OutletRegisterDTO } from '../models/outlet/outlet-register-dto';
import { CustomerDoesntExist } from '../common/exceptions/customer/customer-doesnt-exist.exception';
import { OutletAlreadyExists } from '../common/exceptions/outlet/outlet-already-exists.exception';
import { ShowOutletDTO } from '../models/outlet/show-outlet-dto';
import { UserNotFoundException } from '../common/exceptions/user/user-not-found.exception';
import { ShowAllUsersDTO } from '../models/user/show-all-users.dto';
import { UserActivity } from '../data/entities/user-activity';
import { UserActivityDTO } from '../models/user/activity-dto';
import { Administrator } from '../data/entities/administrator';
import { AdminActivity } from '../data/entities/admin-activity';
import { ShowUserWithOutletDTO } from '../models/user/show-user-with-outlet.dto';
import { async } from 'rxjs/internal/scheduler/async';
@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Customer)
    private readonly customerRepository: Repository<Customer>,
    @InjectRepository(Outlet)
    private readonly outletRepository: Repository<Outlet>,
    @InjectRepository(UserActivity)
    private readonly userActivityRepository: Repository<UserActivity>,
    @InjectRepository(AdminActivity)
    private readonly adminActivityRepository: Repository<AdminActivity>,
    private readonly commonService: CommonService,
  ) {}

  // users
  public async getAllUsers(): Promise<ShowAllUsersDTO[]> {
    const result = await this.userRepository.find({
      where: { isDeleted: false },
    });
    return plainToClass(ShowAllUsersDTO, result, {
      excludeExtraneousValues: true,
    });
  }

  public async getUser(id: string): Promise<ShowUserWithOutletDTO> {
    const foundUser: User = await this.commonService.findUserById(id);
    const outlet = (await foundUser.outlet).name;
    const userwithOutlet = { ...foundUser, outlet };
    return plainToClass(ShowUserWithOutletDTO, userwithOutlet, {
      excludeExtraneousValues: true,
    });
  }

  public async getAllOutlets(): Promise<ShowOutletDTO[]> {
    const result: Outlet[] = await this.outletRepository.find({
      where: { isDeleted: false },
    });

    const mapResult = await Promise.all(
      result.map(async outlet => {
        const newBrand = (await outlet.brand).name;
        const outletWithBrand = { ...outlet, newBrand };
        return await outletWithBrand;
      }),
    );

    return plainToClass(ShowOutletDTO, mapResult, {
      excludeExtraneousValues: true,
    });
  }

  public async getOutlet(id: string): Promise<ShowOutletDTO> {
    return plainToClass(
      ShowOutletDTO,
      await this.commonService.findOutletById(id),
      { excludeExtraneousValues: true },
    );
  }

  // // customers

  public async getAllCustomers(): Promise<ShowCustomerDTO[]> {
    const result = await this.customerRepository.find({
      where: { isDeleted: false },
    });
    return plainToClass(ShowAllUsersDTO, result, {
      excludeExtraneousValues: true,
    });
  }

  public async getCustomer(id: string): Promise<ShowCustomerDTO> {
    return plainToClass(
      ShowUserDTO,
      await this.commonService.findCustomerById(id),
      { excludeExtraneousValues: true },
    );
  }

  public async createUser(user: UserRegisterDTO): Promise<ShowUserDTO> {
    const checkIfUserExists = await this.commonService.findUserByEmail(
      user.email,
    );
    const checkIfOutletExists = await this.commonService.findOutletByName(
      user.outlet,
    );

    if (!!checkIfUserExists) {
      throw new UserAlreadyExistException(
        'User with such email already exists!',
      );
    }

    if (!checkIfOutletExists) {
      throw new OutletNotFoundException(
        `Outlet with given name doesn't exist yet!`,
      );
    }

    const newUser = new User();
    newUser.name = user.name;
    newUser.phone = user.phone;
    newUser.email = user.email;
    newUser.outlet = Promise.resolve(checkIfOutletExists);
    newUser.password = await this.commonService.hashPassword(user.password);

    const createdUser = await this.userRepository.save(newUser);
    return plainToClass(ShowUserDTO, createdUser, {
      excludeExtraneousValues: true,
    });
  }

  public async createCustomer(customer: CustomerRegisterDTO) {
    const checkIfCustomerExists = await this.commonService.findCustomerByName(
      customer.name,
    );

    if (!!checkIfCustomerExists) {
      throw new CustomerAlreadyExists(
        'Customer with same name already exists!',
      );
    }

    const createdCustomer = await this.customerRepository.save(customer);

    return plainToClass(ShowCustomerDTO, createdCustomer, {
      excludeExtraneousValues: true,
    });
  }

  public async createOutlet(outlet: OutletRegisterDTO): Promise<ShowOutletDTO> {
    const checkIfOutletExists = await this.commonService.findOutletByName(
      outlet.name,
    );
    const checkIfCustomerExists = await this.commonService.findCustomerByName(
      outlet.brand,
    );

    if (!!checkIfOutletExists) {
      throw new OutletAlreadyExists('Outlet with same name already exists!');
    }

    if (!checkIfCustomerExists) {
      throw new CustomerDoesntExist(
        `Customer with this name doesn't exists! Please consider creating it first`,
      );
    }

    const newOutlet = new Outlet();
    newOutlet.name = outlet.name;
    newOutlet.brand = Promise.resolve(checkIfCustomerExists);

    const createdOutlet = await this.outletRepository.save(newOutlet);

    return plainToClass(ShowOutletDTO, createdOutlet, {
      excludeExtraneousValues: true,
    });
  }

  public async updateUser(
    id: string,
    user: Partial<UserRegisterDTO>,
  ): Promise<ShowUserDTO> {
    const userFound: User = await this.commonService.findUserById(id);

    if (!userFound) {
      throw new UserNotFoundException(`User with given id doesn't exist`);
    }

    if (!!user.name) {
      userFound.name = user.name;
    }

    if (!!user.email) {
      if (user.email !== userFound.email) {
        const checkIfUserExists = await this.commonService.findUserByEmail(
          user.email,
        );
        if (!!checkIfUserExists) {
          throw new UserAlreadyExistException(
            'User with such email already exists!',
          );
        }
        userFound.email = user.email;
      }
    }

    if (!!user.password) {
      userFound.password = user.password;
    }

    if (!!user.outlet) {
      const checkIfOutletExists = await this.commonService.findOutletByName(
        user.outlet,
      );
      if (!checkIfOutletExists) {
        throw new OutletNotFoundException(
          `Outlet with given name doesn't exist yet!`,
        );
      }
      userFound.outlet = Promise.resolve(checkIfOutletExists);
    }

    const updatedUser = await this.userRepository.save(userFound);
    return plainToClass(ShowUserDTO, updatedUser, {
      excludeExtraneousValues: true,
    });
  }

  public async updateOutlet(
    id: string,
    outlet: Partial<OutletRegisterDTO>,
  ): Promise<ShowOutletDTO> {
    const foundOutlet = await this.commonService.findOutletById(id);

    if (!foundOutlet) {
      throw new OutletNotFoundException(`Outlet with given ID doesn't exist!`);
    }

    if (!!outlet.name) {
      if (outlet.name !== foundOutlet.name) {
        const checkIfOutletExists = await this.commonService.findOutletByName(
          outlet.name,
        );
        if (!!checkIfOutletExists) {
          throw new OutletAlreadyExists(
            'Outlet with same name already exists!',
          );
        }
      }

      foundOutlet.name = outlet.name;
    }

    if (!!outlet.brand) {
      const checkIfCustomerExists = await this.commonService.findCustomerByName(
        outlet.brand,
      );
      if (!checkIfCustomerExists) {
        throw new CustomerDoesntExist(
          `Customer with this name doesn't exists! Please consider creating it first`,
        );
      }

      foundOutlet.brand = Promise.resolve(checkIfCustomerExists);
    }

    const updatedOutlet = await this.outletRepository.save(foundOutlet);
    return plainToClass(ShowOutletDTO, updatedOutlet, {
      excludeExtraneousValues: true,
    });
  }

  public async updateCustomer(id: string, customer: CustomerRegisterDTO) {
    const foundCustomer = await this.customerRepository.findOne({ id });

    if (!foundCustomer) {
      throw new CustomerDoesntExist(`Customer with given ID doesn't exist`);
    }

    if (customer.name) {
      if (foundCustomer.name !== customer.name) {
        const checkIfCustomerExists = await this.commonService.findCustomerByName(
          customer.name,
        );
        if (!!checkIfCustomerExists) {
          throw new CustomerAlreadyExists(
            `Customer with this name already exists`,
          );
        }
      }

      foundCustomer.name = customer.name;
    }

    const updatedCustomer = await this.customerRepository.save(foundCustomer);
    return plainToClass(ShowCustomerDTO, updatedCustomer, {
      excludeExtraneousValues: true,
    });
  }

  public async deleteUser(id: string): Promise<ShowUserDTO> {
    const foundUser = await this.commonService.findUserById(id);

    if (!foundUser) {
      throw new UserNotFoundException(`User with given id doesn't exist`);
    }

    foundUser.isDeleted = true;

    const deletedUser = await this.userRepository.save(foundUser);
    return plainToClass(ShowUserDTO, deletedUser, {
      excludeExtraneousValues: true,
    });
  }

  public async deleteOutlet(id: string): Promise<ShowOutletDTO> {
    const foundOutlet = await this.commonService.findOutletById(id);

    if (!foundOutlet) {
      throw new OutletNotFoundException(`Outlet with given id doesn't exist`);
    }

    foundOutlet.isDeleted = true;

    const deletedOutlet = await this.outletRepository.save(foundOutlet);
    return plainToClass(ShowOutletDTO, deletedOutlet, {
      excludeExtraneousValues: true,
    });
  }

  public async deleteCustomer(id: string): Promise<ShowCustomerDTO> {
    const foundCustomer = await this.customerRepository.findOne({ id });

    if (!foundCustomer) {
      throw new CustomerDoesntExist(`Customer with given id doesn't exist`);
    }

    foundCustomer.isDeleted = true;

    const deletedCustomer = await this.customerRepository.save(foundCustomer);
    return plainToClass(ShowCustomerDTO, deletedCustomer, {
      excludeExtraneousValues: true,
    });
  }

  public async getAllCodesActivity(): Promise<any[]> {
    const allUsers = await this.userRepository.find();
    const allActivities = await allUsers.map(user => {
      return user.redemptionActivities;
    });

    return await allActivities;
  }

  // return await plainToClass(ShowRedemptionCodeActivityDTO, await allActivities, {excludeExtraneousValues:true})
  public async addLoginActivity(user: any): Promise<UserActivityDTO> {
    if (user.priviliges === 'user') {
      return this.addUserActivity(user, 'login');
    }
    if (user.priviliges === 'admin') {
      return this.addAdminActivity(user, 'login');
    }
  }

  public async addLogoutActivity(user: any): Promise<UserActivityDTO> {
    if (user.priviliges === 'user') {
      return this.addUserActivity(user, 'logout');
    }
    if (user.priviliges === 'admin') {
      return this.addAdminActivity(user, 'logout');
    }
  }

  private async addUserActivity(user: User, action: string) {
    const activity = await this.userActivityRepository.create();
    activity.user = Promise.resolve(user);
    const outlet = await user.outlet;
    activity.outlet = outlet.name;
    activity.customer = (await outlet.brand).name;
    if (action === 'login') {
      activity.description = `User ${user.email} logged in`;
    }
    if (action === 'logout') {
      activity.description = `User ${user.email} logged out`;
    }
    const savedActivity = await this.userActivityRepository.save(activity);
    return plainToClass(UserActivityDTO, savedActivity, {
      excludeExtraneousValues: true,
    });
  }

  private async addAdminActivity(user: Administrator, action: string) {
    const activity = await this.adminActivityRepository.create();
    activity.admin = Promise.resolve(user);
    if (action === 'login') {
      activity.description = `Administrator ${user.email} logged in`;
    }
    if (action === 'logout') {
      activity.description = `Administrator ${user.email} logged out`;
    }
    const savedActivity = await this.adminActivityRepository.save(activity);
    return plainToClass(UserActivityDTO, savedActivity, {
      excludeExtraneousValues: true,
    });
  }

  public async getAllUserActivities(): Promise<UserActivityDTO[]> {
    const allActivities: UserActivity[] = await this.userActivityRepository.find();

    const allEditedActivities = await Promise.all(
      allActivities.map(async (activity: UserActivity) => {
        const visibleUser = (await activity.user).name;
        const activityWithUser = { ...activity, visibleUser };

        return await activityWithUser;
      }),
    );

    return plainToClass(UserActivityDTO, allEditedActivities, {
      excludeExtraneousValues: true,
    });
  }
}
