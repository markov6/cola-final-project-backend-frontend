import { Injectable } from '@nestjs/common';
import * as nodemailer from 'nodemailer';

@Injectable()
export class SharedService {
  // async..await is not allowed in global scope, must use a wrapper
  public async sendEmail(sender: string, subject: string, message: string) {
    // create reusable transporter object using the default SMTP transport
    const transporter = nodemailer.createTransport({
      host: 'smtp.sendgrid.net',
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
        user: 'apikey', // https://app.sendgrid.com
        pass:
          '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', // https://app.sendgrid.com
      },
    });

    // send mail with defined transport object
    const info = await transporter.sendMail({
      from: `${sender}`, // sender address
      to: 'TelerikDemoCola@gmail.com', // list of receivers
      subject, // Subject line
      text: `${message}`, // plain text body
      // html: `<b>${content}</b>`, // html body
    });
    return {
      status: 'OK',
      messageId: info.messageId,
    };
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
  }
}
