import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, ObservableLike } from 'rxjs';
import { CreateCustomer } from 'src/app/common/models/create-customer';
import { Customer } from 'src/app/common/models/customer';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  public customer = localStorage.getItem('customer');
  constructor(private readonly http: HttpClient) {}

  public getAllCustomers(): Observable<Customer[]> {
    return this.http.get<Customer[]>(`http://localhost:3000/api/customers`);
  }

  public getCustomer(id: string): Observable<Customer> {
    return this.http.get<Customer>(`http://localhost:3000/api/customers/${id}`);
  }

  public createCustomer(customer: CreateCustomer): Observable<Customer> {
    return this.http.post<Customer>(
      `http://localhost:3000/api/customers`,
      customer,
    );
  }

  public deleteCustomer(id: string): Observable<Customer> {
    return this.http.delete<Customer>(
      `http://localhost:3000/api/customers/${id}`,
    );
  }

  public updateCustomer(
    id: string,
    customer: CreateCustomer,
  ): Observable<Customer> {
    return this.http.put<Customer>(
      `http://localhost:3000/api/customers/${id}`,
      customer,
    );
  }
}
