import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Prize } from 'src/app/common/models/prize';

@Injectable({
  providedIn: 'root',
})
export class CodeCheckingService {
  constructor(private readonly http: HttpClient) {}

  public checkCode(code: string): Observable<Prize> {
    return this.http.get<Prize>(`http://localhost:3000/api/codes/${code}`);
  }

  public getAllWinningCodes(): Observable<Prize[]> {
    return this.http.get<Prize[]>(
      `http://localhost:3000/api/codes/allwinningcodes`
    );
  }
}
