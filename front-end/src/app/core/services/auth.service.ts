import { HttpClient } from '@angular/common/http';
import { error } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UserLogin } from 'src/app/common/models/user-login';
import { StorageService } from './storage.service';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public get user$() {
    return this.userSubject$.asObservable();
  }

  public get role$() {
    return this.userRoleSubject$.asObservable();
  }

  public get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  public get userId$(): Observable<string> {
    return this.userIdSubject$.asObservable();
  }

  private get username(): string | null {
    const token = this.storage.getItem('token');
    const username = this.storage.getItem('username') || '';
    if (token) {
      return username;
    }

    return null;
  }

  private get role(): string | null {
    const token = this.storage.getItem('token');
    const role = this.storage.getItem('role') || '';
    if (token) {
      return role;
    }

    return null;
  }

  private get userId(): string | null {
    const token = this.storage.getItem('token');
    const id = this.storage.getItem('id') || '';
    if (token) {
      console.log(`returning id now ${id}`);
      return id;
    }
    console.log('returning null for userId');
    return null;
  }
  private isLoggedInSubject$ = new BehaviorSubject<boolean>(
    this.isUserAuthenticated(),
  );
  private readonly userSubject$ = new BehaviorSubject<string | null>(
    this.username,
  );
  private readonly userRoleSubject$ = new BehaviorSubject<string | null>(
    this.role,
  );
  private readonly userIdSubject$ = new BehaviorSubject<string | null>(
    this.userId,
  );

  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly userService: UsersService,
  ) {}

  public register(
    firstName: string,
    lastName: string,
    username: string,
    email: string,
    password: string,
  ) {
    return this.http.post('http://localhost:3000/api/user/register', {
      firstName,
      lastName,
      username,
      email,
      password,
    });
  }

  public login(email: string, password: string) {
    return this.http
      .post('http://localhost:3000/api/auth', {
        email,
        password,
      })
      .pipe(
        tap((res: any) => {
          this.userSubject$.next(res.user.name);
          this.userRoleSubject$.next(res.user.userrole);
          this.storage.setItem('token', res.token);
          this.storage.setItem('role', res.user.userrole);
          this.storage.setItem('username', res.user.username);
          this.storage.setItem('id', res.user.id);
          this.isLoggedInSubject$.next(true);
          this.userIdSubject$.next(res.user.id);
          this.userService.addLoginActivity().subscribe();
        }),
      );
  }

  public loginAdmin(email: string, password: string) {
    return this.http
      .post('http://localhost:3000/api/auth/admin', {
        email,
        password,
      })
      .pipe(
        tap((res: any) => {
          this.userSubject$.next(res.user.name);
          this.userRoleSubject$.next(res.user.userrole);
          this.storage.setItem('token', res.token);
          this.storage.setItem('role', res.user.userrole);
          this.storage.setItem('username', res.user.username);
          this.storage.setItem('id', res.user.id);
          this.userIdSubject$.next(res.user.id);
          this.isLoggedInSubject$.next(true);
          this.userService.addLoginActivity().subscribe();
        }),
      );
  }

  public logout(): void {
    this.userService.addLogoutActivity().subscribe();
    this.storage.removeItem('token');
    this.storage.removeItem('username');
    this.storage.removeItem('role');
    this.storage.removeItem('id');
    // this.storage.removeItem('theme');
    this.userSubject$.next(null);
    this.isLoggedInSubject$.next(false);
    this.userIdSubject$.next(null);
  }

  private isUserAuthenticated(): boolean {
    return !!this.storage.getItem('token');
  }
}
