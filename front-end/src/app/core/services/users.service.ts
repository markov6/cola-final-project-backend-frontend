import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { CreateUser } from 'src/app/common/models/create-user';
import { User } from 'src/app/common/models/user';
import { UserActivities } from 'src/app/common/models/user-activities';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  public username = localStorage.getItem('username');
  constructor(private readonly http: HttpClient) {}

  public getAllUsers(search?: string): Observable<User[]> {
    return this.http.get<User[]>('http://localhost:3000/api/users');
  }

  public getUser(id: string): Observable<User> {
    return this.http.get<User>(`http://localhost:3000/api/users/${id}`);
  }

  public deleteUser(id: string): Observable<User> {
    return this.http.delete<User>(`http://localhost:3000/api/users/${id}`);
  }

  public updateUser(id: string, user: CreateUser): Observable<User> {
    return this.http.put<User>(`http://localhost:3000/api/users/${id}`, user);
  }

  public createUser(user: CreateUser): Observable<User> {
    return this.http.post<User>(`http://localhost:3000/api/users`, user);
  }

  public addLoginActivity(): Observable<User> {
    return this.http.post<User>(
      `http://localhost:3000/api/users/loginActivity`,
      {}
    );
  }

  public addLogoutActivity(): Observable<User> {
    return this.http.post<User>(
      `http://localhost:3000/api/users/logoutActivity`,
      {}
    );
  }

  public getAllUserActivities(): Observable<UserActivities[]> {
    return this.http.get<UserActivities[]>(
      `http://localhost:3000/api/allUserActivities`
    );
  }
}
