import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { HomeComponent } from '../components/home/home.component';
import { CheckCodeComponent } from './components/check-code/check-code.component';
import { OutletActivitiesComponent } from './components/outlet-activities/outlet-activities.component';
import { PrizeScannerComponent } from './components/prize-scanner/prize-scanner.component';
import { ScannerComponent } from './components/scanner/scanner.component';
import { UserActivitiesComponent } from './components/user-activities/user-activities.component';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { MyActivitiesResolverService } from './services/my-activities-resolver.service';
import { OutletActivitiesResolverService } from './services/outlet-activities-resolver.service';

const routes: Routes = [
  { path: '', component: UserLoginComponent, pathMatch: 'full' },
  {
    path: 'profile/:id',
    component: UserProfileComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'activities/:id',
    component: UserActivitiesComponent,
    canActivate: [AuthGuard],
    resolve: {
      activities: MyActivitiesResolverService,
    },
  },
  {
    path: 'outletactivities/:id',
    component: OutletActivitiesComponent,
    canActivate: [AuthGuard],
    resolve: {
      activities: OutletActivitiesResolverService,
    },
  },
  // {path: 'profile/:id', component: ProfileComponent,  resolve: {user: SingleUserResolverService }}
  { path: 'code/:id', component: CheckCodeComponent, canActivate: [AuthGuard] },
  {
    path: 'prize/:id',
    component: PrizeScannerComponent,
    canActivate: [AuthGuard],
  },
  { path: 'scanner', component: ScannerComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {}
