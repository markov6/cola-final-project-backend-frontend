import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { TokenInterceptorService } from '../auth/token-interceptor.service';
import { AutofocusDirective } from '../common/directives/auto-focus.directive';
import { HelpdeskComponent } from '../components/helpdesk/helpdesk.component';
import { SharedModule } from '../shared/shared.module';
import { AlreadyRedeemedDialogComponent } from './components/already-redeemed-dialog/already-redeemed-dialog.component';
import { CheckCodeComponent } from './components/check-code/check-code.component';
import { OutletActivitiesComponent } from './components/outlet-activities/outlet-activities.component';
import { PrizeScanDialogComponent } from './components/prize-scan-dialog/prize-scan-dialog.component';
import { PrizeScannerComponent } from './components/prize-scanner/prize-scanner.component';
import { ReportRedemptionDialogComponent } from './components/report-redemption-dialog/report-redemption-dialog.component';
import { ScannerComponent } from './components/scanner/scanner.component';
import { UserActivitiesComponent } from './components/user-activities/user-activities.component';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { UsersRoutingModule } from './users-routing.module';
@NgModule({
  declarations: [
    UserLoginComponent,
    ScannerComponent,
    UserProfileComponent,
    UserActivitiesComponent,
    OutletActivitiesComponent,
    CheckCodeComponent,
    PrizeScanDialogComponent,
    PrizeScannerComponent,
    AlreadyRedeemedDialogComponent,
    AutofocusDirective,
    ReportRedemptionDialogComponent,
  ],

  imports: [CommonModule, SharedModule, UsersRoutingModule, ZXingScannerModule],
  entryComponents: [
    PrizeScanDialogComponent,
    AlreadyRedeemedDialogComponent,
    ReportRedemptionDialogComponent,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
  ],
})
export class UsersModule {}
