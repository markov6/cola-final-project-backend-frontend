import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { InitialCodeLog } from 'src/app/common/models/initial-code-log';
import { PrizeUpdate } from 'src/app/common/models/prize-update';
import { RedemptionLog } from 'src/app/common/models/redemption-log';

@Injectable({
  providedIn: 'root',
})
export class RedemptionActivitiesService {
  constructor(private readonly http: HttpClient) {}
  /// initial scan created by each "winning" barcode scan operation
  public saveScan(
    code: string,
    winning: boolean,
    redeemed: boolean
  ): Observable<any> {
    console.log(redeemed);
    return this.http.post<InitialCodeLog>(
      `http://localhost:3000/api/codes/scanned/${code}`,
      { code, winning, redeemed }
    );
  }

  //  updates initially created on scan record with appropriate redemptions status
  public redeem(id: string, code: string): Observable<any> {
    return this.http.put<RedemptionLog>(
      `http://localhost:3000/api/codes/scanned/${code}`,
      { id }
    );
  }

  //  gets already redeemed code details
  public getRedeemedCodeDetails(code: string): Observable<any> {
    return this.http.get<any>(
      `http://localhost:3000/api/codes/scanned/${code}`
    );
  }

  //  redeems code with prize record
  public redeemWithPrize(
    activityId: string,
    prizeBarcode: string
  ): Observable<any> {
    return this.http.put<PrizeUpdate>(
      `http://localhost:3000/api/codes/activities/${activityId}`,
      { prizeBarcode }
    );
  }

  // reports already redeemed code to Admin
  public reportRedemptionAttempt(
    userId: string,
    winningCode: string
  ): Observable<any> {
    return this.http.post<any>(
      `http://localhost:3000/api/codes/email/${winningCode}`,
      {}
    );
  }
  // get allRedemptionActivities

  public getAllRedemptionActivities(): Observable<any> {
    return this.http.get<any>(
      `http://localhost:3000/api/codes/redemptionactivities`
    );
  }

  public getUserRedemptionActivities(userId: string): Observable<any> {
    return this.http.get<any>(
      `http://localhost:3000/api/codes/redemptionactivities/?userId=${userId}`
    );
  }

  public getOutletRedemptionActivities(outlet: string): Observable<any> {
    return this.http.get<any>(
      `http://localhost:3000/api/codes/redemptionactivities/?outlet=${outlet}`
    );
  }
}
