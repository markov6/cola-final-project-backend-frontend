import { TestBed } from '@angular/core/testing';

import { RedemptionActivitiesService } from './redemption-activities.service';

describe('RedemptionActivitiesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RedemptionActivitiesService = TestBed.get(
      RedemptionActivitiesService
    );
    expect(service).toBeTruthy();
  });
});
