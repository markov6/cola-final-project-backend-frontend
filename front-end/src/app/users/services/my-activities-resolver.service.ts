import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { RedemptionActivitiesService } from './redemption-activities.service';

@Injectable({
  providedIn: 'root',
})
export class MyActivitiesResolverService {
  constructor(
    private readonly redemptionActivitiesService: RedemptionActivitiesService,
    private readonly notificator: NotificatorService
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.redemptionActivitiesService
      .getUserRedemptionActivities(route.params.id)
      .pipe(
        catchError(res => {
          this.notificator.error(res.error.error);
          return of({ activities: [] });
        })
      );
  }
}
