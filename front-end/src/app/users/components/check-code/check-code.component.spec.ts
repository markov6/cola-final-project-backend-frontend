import {
  async,
  ComponentFixture,
  TestBed,
  ComponentFixtureAutoDetect,
} from '@angular/core/testing';

import { CheckCodeComponent } from './check-code.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule } from 'ngx-toastr';
import { RedemptionActivitiesService } from '../../services/redemption-activities.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { CodeCheckingService } from 'src/app/core/services/code-checking.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of, Observable } from 'rxjs';
import { By } from '@angular/platform-browser';

fdescribe('CheckCodeComponent with invalid code', () => {
  let component: CheckCodeComponent;
  let fixture: ComponentFixture<CheckCodeComponent>;

  const auth = jasmine.createSpyObj('AuthService', ['userId$', 'logout']);
  const router = jasmine.createSpyObj('Router', [
    'navigate',
    'createUrlTree',
    'navigateByUrl',
  ]);
  const activatedRoute = jasmine.createSpyObj('ActivatedRoute', ['']);
  const dialog = jasmine.createSpyObj('MatDialog', ['open']);
  const codeChecker = jasmine.createSpyObj('CodeCheckingService', [
    'checkCode',
  ]);
  const notificator = jasmine.createSpyObj('NotificatorService', ['success']);
  const redemptionService = jasmine.createSpyObj(
    'RedemptionActivitiesService',
    ['saveScan'],
  );

  activatedRoute.params = of({
    id: '123123123',
  });

  auth.userId$ = of(123123);

  codeChecker.checkCode.and.returnValue(
    of({
      barcode: '123123123',
      prize: 'Not winning!',
      redeemed: false,
    }),
  );

  redemptionService.saveScan.and.returnValue(of(true));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CheckCodeComponent],
      imports: [
        SharedModule,
        RouterTestingModule,
        ToastrModule.forRoot(),
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: ComponentFixtureAutoDetect,
          useValue: true,
        },
        {
          provide: AuthService,
          useValue: auth,
        },
        {
          provide: Router,
          useValue: router,
        },
        {
          provide: ActivatedRoute,
          useValue: activatedRoute,
        },
        {
          provide: MatDialog,
          useValue: dialog,
        },
        {
          provide: CodeCheckingService,
          useValue: codeChecker,
        },
        {
          provide: NotificatorService,
          useValue: notificator,
        },
        {
          provide: RedemptionActivitiesService,
          useValue: redemptionService,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('not winning card should be displayed', () => {
    const el = fixture.nativeElement;
    expect(el.innerText).toContain('Barcode 123123123');
    expect(el.innerText).toContain('Barcode 123123123 is not winning!');
    expect(el.innerText).toContain('User can scan another code');
    expect(el.innerText).toContain('SCAN ANOTHER CODE');
    expect(el.innerText).toContain('LOG OUT');
  });

  it('should render code denied image', () => {
    const img = fixture.nativeElement.querySelector('img');
    expect(img.src).toContain('denied.png');
  });

  it('should have two buttons rendered', () => {
    const buttons = fixture.debugElement.queryAllNodes(By.css('button'));
    expect(buttons.length).toEqual(2);
  });

  it('should have called the redemptionService for initial scan', () => {
    expect(redemptionService.saveScan).toHaveBeenCalledWith(
      '123123123',
      false,
      false,
    );
  });

  it(`"logout" click should should call logout()`, () => {
    const button = fixture.debugElement.query(By.css('#logout'));
    const logout = spyOn(component, 'logout');
    button.triggerEventHandler('click', null);
    expect(component.logout).toHaveBeenCalledTimes(1);
  });

  it(`"logout" click should should call auth.logout()`, () => {
    const button = fixture.debugElement.query(By.css('#logout'));
    button.triggerEventHandler('click', null);
    expect(auth.logout).toHaveBeenCalledTimes(1);
    expect(notificator.success).toHaveBeenCalledTimes(1);
    expect(notificator.success).toHaveBeenCalledWith(`You have logged out.`);
  });
});

fdescribe('CheckCodeComponent with winning code', () => {
  let component: CheckCodeComponent;
  let fixture: ComponentFixture<CheckCodeComponent>;

  const auth = jasmine.createSpyObj('AuthService', ['userId$']);
  const router = jasmine.createSpyObj('Router', [
    'navigate',
    'createUrlTree',
    'navigateByUrl',
  ]);
  const activatedRoute = jasmine.createSpyObj('ActivatedRoute', ['']);
  const dialog = jasmine.createSpyObj('MatDialog', ['open']);
  const codeChecker = jasmine.createSpyObj('CodeCheckingService', [
    'checkCode',
  ]);
  const notificator = jasmine.createSpyObj('NotificatorService', ['success']);
  const redemptionService = jasmine.createSpyObj(
    'RedemptionActivitiesService',
    ['saveScan', 'redeem'],
  );

  activatedRoute.params = of({
    id: '123123123',
  });

  auth.userId$ = of(123123);

  codeChecker.checkCode.and.returnValue(
    of({
      barcode: '123123123',
      prize: 'Soft Drink Can - 0.333 l',
      redeemed: false,
    }),
  );

  dialog.open.and.returnValue({
    afterClosed: () => of(false),
  });

  redemptionService.redeem.and.returnValue(of(true));
  redemptionService.saveScan.and.returnValue(of(true));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CheckCodeComponent],
      imports: [
        SharedModule,
        RouterTestingModule,
        ToastrModule.forRoot(),
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: ComponentFixtureAutoDetect,
          useValue: true,
        },
        {
          provide: AuthService,
          useValue: auth,
        },
        {
          provide: Router,
          useValue: router,
        },
        {
          provide: ActivatedRoute,
          useValue: activatedRoute,
        },
        {
          provide: MatDialog,
          useValue: dialog,
        },
        {
          provide: CodeCheckingService,
          useValue: codeChecker,
        },
        {
          provide: NotificatorService,
          useValue: notificator,
        },
        {
          provide: RedemptionActivitiesService,
          useValue: redemptionService,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('winning  and not yet redeemed card should be displayed', () => {
    const el = fixture.nativeElement;
    expect(el.innerText).toContain('Barcode 123123123');
    expect(el.innerText).toContain('Barcode 123123123 is winning!');
    expect(el.innerText).toContain('Prize type: "Soft Drink Can - 0.333 l');
    expect(el.innerText).toContain('REDEEM CODE NOW');
    expect(el.innerText).toContain('SCAN ANOTHER CODE');
  });

  it('should render speficic prize image', () => {
    const img = fixture.nativeElement.querySelector('img');
    expect(img.src).toContain('Soft%20Drink%20Can%20-%200.333%20l.jpg');
  });

  it('should have two buttons rendered', () => {
    const buttons = fixture.debugElement.queryAllNodes(By.css('button'));
    expect(buttons.length).toEqual(2);
  });

  it('should have called the redemptionService for initial scan record with correct parameters', () => {
    expect(redemptionService.saveScan).toHaveBeenCalledWith(
      '123123123',
      true,
      false,
    );
  });

  it(`"redeem code now" click should should call MatDialog openDialog (with mocked NO response)`, () => {
    const button = fixture.debugElement.query(By.css('#redeem'));
    button.triggerEventHandler('click', null);
    expect(dialog.open).toHaveBeenCalledTimes(1);
    expect(redemptionService.redeem).toHaveBeenCalledTimes(1);
    expect(notificator.success).toHaveBeenCalledWith(
      `Code 123123123 was succesfully redeemed`,
    );
  });

  it(`"redeem code now" click should call redeem()`, () => {
    const button = fixture.debugElement.query(By.css('#redeem'));
    const redeem = spyOn(component, 'redeem');
    button.triggerEventHandler('click', null);
    expect(component.redeem).toHaveBeenCalledTimes(1);
  });

  it(`"redeem code now" click should should call component openDialog()`, () => {
    const button = fixture.debugElement.query(By.css('#redeem'));
    const openAlreadyRedeemedDialog = spyOn(component, 'openDialog');
    button.triggerEventHandler('click', null);
    expect(component.openDialog).toHaveBeenCalledTimes(1);
  });
});

fdescribe('CheckCodeComponent with redeemed code', () => {
  let component: CheckCodeComponent;
  let fixture: ComponentFixture<CheckCodeComponent>;

  const auth = jasmine.createSpyObj('AuthService', ['userId$']);
  const router = jasmine.createSpyObj('Router', [
    'navigate',
    'createUrlTree',
    'navigateByUrl',
  ]);
  const activatedRoute = jasmine.createSpyObj('ActivatedRoute', ['']);
  const dialog = jasmine.createSpyObj('MatDialog', ['open', 'afterClosed']);
  const codeChecker = jasmine.createSpyObj('CodeCheckingService', [
    'checkCode',
  ]);
  const notificator = jasmine.createSpyObj('NotificatorService', ['success']);
  const dialogRef = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
  const redemptionService = jasmine.createSpyObj(
    'RedemptionActivitiesService',
    ['saveScan', 'reportRedemptionAttempt', 'getRedeemedCodeDetails'],
  );

  activatedRoute.params = of({
    id: '123123123',
  });

  auth.userId$ = of(123123);

  codeChecker.checkCode.and.returnValue(
    of({
      barcode: '123123123',
      prize: 'Soft Drink Can - 0.333 l',
      redeemed: true,
    }),
  );

  dialog.open.and.returnValue({
    afterClosed: () => of(true),
  });

  redemptionService.reportRedemptionAttempt.and.returnValue(of('ok'));

  redemptionService.saveScan.and.returnValue(of(true));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CheckCodeComponent],
      imports: [
        SharedModule,
        RouterTestingModule,
        ToastrModule.forRoot(),
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: ComponentFixtureAutoDetect,
          useValue: true,
        },
        {
          provide: MatDialogRef,
          useValue: dialog,
        },
        {
          provide: AuthService,
          useValue: auth,
        },
        {
          provide: Router,
          useValue: router,
        },
        {
          provide: ActivatedRoute,
          useValue: activatedRoute,
        },
        {
          provide: MatDialog,
          useValue: dialog,
        },
        {
          provide: CodeCheckingService,
          useValue: codeChecker,
        },
        {
          provide: NotificatorService,
          useValue: notificator,
        },
        {
          provide: RedemptionActivitiesService,
          useValue: redemptionService,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('winning and already redeemed card should be displayed', () => {
    const el = fixture.nativeElement;
    expect(el.innerText).toContain('Barcode 123123123');
    expect(el.innerText).toContain('Barcode 123123123 is winning!');
    expect(el.innerText).toContain('This code was already redeemed');
    expect(el.innerText).toContain('REPORT REDEMPTION ATTEMPT');
    expect(el.innerText).toContain('CHECK REDEMPTION DETAILS');
    expect(el.innerText).toContain('SCAN ANOTHER CODE');
  });

  it('should render denied image', () => {
    const img = fixture.nativeElement.querySelector('img');
    expect(img.src).toContain('denied.png');
  });

  it('should have three buttons rendered', () => {
    const buttons = fixture.debugElement.queryAllNodes(By.css('button'));
    expect(buttons.length).toEqual(3);
  });

  it('should have called the redemptionService for initial scan record with correct parameters', () => {
    expect(redemptionService.saveScan).toHaveBeenCalledWith(
      '123123123',
      true,
      true,
    );
  });

  it(`"report redemption attempt" click should should call openDialog()`, () => {
    const button = fixture.debugElement.query(By.css('#reportRedemption'));
    button.triggerEventHandler('click', null);
    expect(dialog.open).toHaveBeenCalledTimes(1);
  });

  it(`"report redemption attempt" click should should call reportRedemptionAttempt()`, () => {
    const button = fixture.debugElement.query(By.css('#reportRedemption'));
    const reportRedemptionAttempt = spyOn(component, 'reportRedemptionAttempt');
    button.triggerEventHandler('click', null);
    expect(component.reportRedemptionAttempt).toHaveBeenCalledTimes(1);
  });

  it(`"report redemption attempt" click should should call rerortredemptionService after dialog confirmation()`, () => {
    const button = fixture.debugElement.query(By.css('#reportRedemption'));
    redemptionService.reportRedemptionAttempt.calls.reset();
    notificator.success.calls.reset();
    button.triggerEventHandler('click', null);
    expect(redemptionService.reportRedemptionAttempt).toHaveBeenCalledTimes(1);
    expect(redemptionService.reportRedemptionAttempt).toHaveBeenCalledWith(
      123123,
      '123123123',
    );
    expect(notificator.success).toHaveBeenCalledTimes(1);
  });

  it(`"check redemption details" click should should call openDialog()`, () => {
    const button = fixture.debugElement.query(By.css('#checkRedemption'));
    dialog.open.calls.reset();
    button.triggerEventHandler('click', null);
    expect(dialog.open).toHaveBeenCalledTimes(1);
    expect(redemptionService.getRedeemedCodeDetails).toHaveBeenCalledTimes(1);
    expect(redemptionService.getRedeemedCodeDetails).toHaveBeenCalledWith(
      '123123123',
    );
  });

  it(`"check redemption details" click should should call openAlreadyRedeemedDialog()`, () => {
    const button = fixture.debugElement.query(By.css('#checkRedemption'));
    const openAlreadyRedeemedDialog = spyOn(
      component,
      'openAlreadyRedeemedDialog',
    );
    button.triggerEventHandler('click', null);
    expect(component.openAlreadyRedeemedDialog).toHaveBeenCalledTimes(1);
  });
});
