import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrizeScanDialogComponent } from './prize-scan-dialog.component';

describe('PrizeScanDialogComponent', () => {
  let component: PrizeScanDialogComponent;
  let fixture: ComponentFixture<PrizeScanDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrizeScanDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrizeScanDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
