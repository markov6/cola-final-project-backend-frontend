import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { StorageService } from 'src/app/core/services/storage.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss'],
})
export class UserLoginComponent implements OnInit {
  public loginForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly storageService: StorageService,
    private readonly formBuilder: FormBuilder
  ) {}

  public ngOnInit() {
    this.loginForm = this.formBuilder.group({
      emailValue: ['', [Validators.required, Validators.email]],
      passwordValue: [
        '',
        [
          Validators.required,
          Validators.pattern(
            /(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/
          ),
        ],
      ],
    });
  }

  get emailValue() {
    return this.loginForm.get('emailValue');
  }

  get passwordValue() {
    return this.loginForm.get('passwordValue');
  }

  public triggerLogin(email: string, password: string) {
    this.authService.login(email, password).subscribe(
      () => {
        this.storageService.getItem('username');
        this.notificator.success(
          `Welcome, ${this.storageService.getItem('username')}!`
        );
        this.router.navigate([
          `/users/profile/${this.storageService.getItem('id')}`,
        ]);
      },
      error => this.notificator.error(error.error.error)
    );
  }
}
