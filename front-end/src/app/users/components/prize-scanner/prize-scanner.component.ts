import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BarcodeFormat } from '@zxing/library';
import { BehaviorSubject, Subscription } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { RedemptionActivitiesService } from '../../services/redemption-activities.service';

@Component({
  selector: 'app-prize-scanner',
  templateUrl: './prize-scanner.component.html',
  styleUrls: ['./prize-scanner.component.scss'],
})
export class PrizeScannerComponent implements OnInit, OnDestroy {

  get barcodeValue() {
    return this.barcodeForm.get('barcodeValue');
  }
  public redemptionActivityId: string;
  public userId: string;
  public barcodeForm: FormGroup;
  public availableDevices: MediaDeviceInfo[];
  public currentDevice: MediaDeviceInfo = null;

  public formatsEnabled: BarcodeFormat[] = [
    BarcodeFormat.EAN_13,
    BarcodeFormat.QR_CODE,
  ];

  public hasDevices: boolean;
  public hasPermission: boolean;

  public qrResultString: string;

  public torchEnabled = false;
  public torchAvailable$ = new BehaviorSubject<boolean>(false);
  public tryHarder = false;

  public sound = new Audio('assets/barcode.wav');
  private userSubscription: Subscription;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly auth: AuthService,
    private readonly formBuilder: FormBuilder,
    private readonly redemptionSerice: RedemptionActivitiesService,
    private readonly notificator: NotificatorService,
    private readonly router: Router
  ) {}

  public ngOnInit() {
    this.barcodeForm = this.formBuilder.group({
      barcodeValue: ['', [Validators.required, Validators.pattern(/^\d{13}$/)]],
    });
    this.route.params.subscribe(data => (this.redemptionActivityId = data.id));

    this.userSubscription = this.auth.userId$.subscribe(
      data => (this.userId = data)
    );
  }

  public ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  public clearResult(): void {
    this.qrResultString = null;
  }

  public onCamerasFound(devices: MediaDeviceInfo[]): void {
    this.availableDevices = devices;
    this.hasDevices = Boolean(devices && devices.length);
  }

  public onCodeResult(resultString: string) {
    this.sound.play();
    this.qrResultString = resultString;
  }

  public onDeviceSelectChange(selected: string) {
    const device = this.availableDevices.find(x => x.deviceId === selected);
    this.currentDevice = device || null;
  }

  public onHasPermission(has: boolean) {
    this.hasPermission = has;
  }

  public onTorchCompatible(isCompatible: boolean): void {
    this.torchAvailable$.next(isCompatible || false);
  }

  public toggleTorch(): void {
    this.torchEnabled = !this.torchEnabled;
  }

  public redeemWithPrize(barcodeValue: string) {
    this.redemptionSerice
      .redeemWithPrize(this.redemptionActivityId, barcodeValue)
      .subscribe(
        data => {
          this.notificator.success(`Winning code was succesfully redeemed`);
          this.router.navigate([`users/profile/${this.userId}`]);
        },
        error => {
          'There was an error trying to redeem this code! Please contact helpdesk!';
        }
      );
  }
}
