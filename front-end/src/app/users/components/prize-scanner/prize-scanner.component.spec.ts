import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrizeScannerComponent } from './prize-scanner.component';

describe('PrizeScannerComponent', () => {
  let component: PrizeScannerComponent;
  let fixture: ComponentFixture<PrizeScannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrizeScannerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrizeScannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
