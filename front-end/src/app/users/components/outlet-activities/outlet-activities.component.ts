import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { RedemptionActivity } from 'src/app/common/models/redemption-activity';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-outlet-activities',
  templateUrl: './outlet-activities.component.html',
  styleUrls: ['./outlet-activities.component.scss'],
})
export class OutletActivitiesComponent implements OnInit, OnDestroy {
  // this is build in pagination and sorting for MatTableDataSource
  @ViewChild(MatPaginator) public paginator: MatPaginator;
  @ViewChild(MatSort) public sort: MatSort;

  // this is the data used in the table
  public dataSource: MatTableDataSource<any>;
  public pipe: DatePipe;
  public filterString = '';

  public columnsToDisplay = [
    'timestamp',
    'code',
    'outlet',
    'redemptionStatus',
    'prizeCode',
    'username',
  ];

  public redemptionActivities: RedemptionActivity[] = [];
  public username = '';
  public userId = '';
  public filterUser: '';
  public userOutlet = '';

  public filterForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
  });

  private subscription: Subscription;
  private userIdSubscription: Subscription;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly auth: AuthService,
  ) {}

  public ngOnInit() {
    this.subscription = this.auth.user$.subscribe(username => {
      if (username === null) {
        this.username = '';
      } else {
        this.username = username;
      }
    });

    this.userIdSubscription = this.auth.userId$.subscribe(data => {
      this.userId = data;
    });

    this.route.data.subscribe(data => {
      this.dataSource = new MatTableDataSource(data.activities);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.getDateRange();
    });
  }

  public ngOnDestroy(): void {
    this.userIdSubscription.unsubscribe();
    this.subscription.unsubscribe();
  }

  public applyFilter(filterValue: string) {
    this.filterString = filterValue.trim().toLowerCase();

    this.dataSource.filter = filterValue;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  get fromDate() {
    return this.filterForm.get('fromDate').value;
  }

  get toDate() {
    return this.filterForm.get('toDate').value;
  }

  public getDateRange() {
    this.pipe = new DatePipe('en');
    this.dataSource.filterPredicate = (data: RedemptionActivity, filter) => {
      let result = true;
      const prizeCode =
        data.prizeCode.toLowerCase().indexOf(this.filterString.toLowerCase()) >=
        0;
      const code =
        data.code.toLowerCase().indexOf(this.filterString.toLowerCase()) >= 0;
      const redemptionStatus =
        data.redemptionStatus
          .toLowerCase()
          .indexOf(this.filterString.toLowerCase()) >= 0;
      const cashierName =
        data.username.toLowerCase().indexOf(this.filterString.toLowerCase()) >=
        0;

      const outlet =
        data.outlet.toLowerCase().indexOf(this.filterString.toLowerCase()) >= 0;

      if (this.fromDate && this.toDate) {
        const actualDate = Date.parse((data as any).timestamp);
        const fromDate = Date.parse(
          moment(this.fromDate, 'DD/MM/YYYY', true).format(),
        );
        const toDate = Date.parse(
          moment(this.toDate, 'DD/MM/YYYY', true).format(),
        );

        result = actualDate >= fromDate && actualDate <= toDate;
      }

      if (!result) {
        return false;
      }

      if (this.filterString) {
        result =
          (result && code) ||
          (result && prizeCode) ||
          (result && redemptionStatus) ||
          (result && cashierName) ||
          (result && outlet);
      }

      return result;
    };
  }

  public applyDateFilter() {
    this.dataSource.filter = '' + Math.random();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
