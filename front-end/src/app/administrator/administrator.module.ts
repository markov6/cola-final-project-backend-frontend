import { LayoutModule } from '@angular/cdk/layout';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { ChartsModule } from 'ng2-charts';
import { TokenInterceptorService } from '../auth/token-interceptor.service';
import { SharedModule } from '../shared/shared.module';
import { AdministratorRoutingModule } from './administrator-routing.module';
import { AdministratorCustomersCreateComponent } from './components/administrator-customers-create/administrator-customers-create.component';
import { AdministratorCustomersComponent } from './components/administrator-customers/administrator-customers.component';
import { AdministratorDashboardComponent } from './components/administrator-dashboard/administrator-dashboard.component';
import { DashboardBarchartCustomersComponent } from './components/administrator-dashboard/dashboard-barchart-customers/dashboard-barchart-customers.component';
import { DashboardPiechartWinningcodesComponent } from './components/administrator-dashboard/dashboard-piechart-winningcodes/dashboard-piechart-winningcodes.component';
import { DashboardPiechartComponent } from './components/administrator-dashboard/dashboard-piechart/dashboard-piechart.component';
import { PiechartOutletsNumberComponent } from './components/administrator-dashboard/piechart-outlets-number/piechart-outlets-number.component';
import { AdministratorHomeComponent } from './components/administrator-home/administrator-home.component';
import { AdministratorLoginComponent } from './components/administrator-login/administrator-login.component';
import { AdministratorOutletsCreateComponent } from './components/administrator-outlets-create/administrator-outlets-create.component';
import { AdministratorOutletsComponent } from './components/administrator-outlets/administrator-outlets.component';
import { AdministratorReportsComponent } from './components/administrator-reports/administrator-reports.component';
import { AdministratorUserActivitiesComponent } from './components/administrator-user-activities/administrator-user-activities.component';
import { AdministratorUsersCreateComponent } from './components/administrator-users-create/administrator-users-create.component';
import { AdministratorUsersComponent } from './components/administrator-users/administrator-users.component';

@NgModule({
  declarations: [
    AdministratorLoginComponent,
    AdministratorHomeComponent,
    AdministratorUsersComponent,
    AdministratorOutletsComponent,
    AdministratorCustomersComponent,
    AdministratorReportsComponent,
    AdministratorUsersCreateComponent,
    AdministratorOutletsCreateComponent,
    AdministratorCustomersComponent,
    AdministratorCustomersCreateComponent,
    AdministratorDashboardComponent,
    DashboardPiechartComponent,
    DashboardBarchartCustomersComponent,
    DashboardPiechartWinningcodesComponent,
    PiechartOutletsNumberComponent,
    AdministratorUserActivitiesComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    AdministratorRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    ChartsModule,
    MatSelectModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
  ],

  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
  ],
})
export class AdministratorModule {}
