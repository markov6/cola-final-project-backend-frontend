import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-administrator-login',
  templateUrl: './administrator-login.component.html',
  styleUrls: ['./administrator-login.component.scss']
})
export class AdministratorLoginComponent implements OnInit {

  public adminLoginForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
  ) { }

  public ngOnInit() {
  }

  public triggerLogin(username: string, password: string) {
    this.authService.loginAdmin(username, password).subscribe(
      () => {
        this.notificator.success(`Welcome, admin!`);
        this.router.navigate(['administrator/users']);
      },
      error => this.notificator.error(error.error.error),
    );
  }

}
