import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PiechartOutletsNumberComponent } from './piechart-outlets-number.component';

describe('PiechartOutletsNumberComponent', () => {
  let component: PiechartOutletsNumberComponent;
  let fixture: ComponentFixture<PiechartOutletsNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PiechartOutletsNumberComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PiechartOutletsNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
