import { WeekDay } from '@angular/common';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import { RedemptionActivity } from 'src/app/common/models/redemption-activity';

@Component({
  selector: 'app-dashboard-piechart',
  templateUrl: './dashboard-piechart.component.html',
  styleUrls: ['./dashboard-piechart.component.scss'],
})
export class DashboardPiechartComponent implements OnInit, OnChanges {
  @Input() public redemptionActivities: RedemptionActivity[];

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'left',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    },
  };
  public pieChartLabels: Label[] = [];
  public pieChartData: number[] = [];
  public pieChartPlugins = [pluginDataLabels];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartColors = [
    {
      backgroundColor: [
        'rgba(255,0,0,0.3)',
        'rgba(0,255,0,0.3)',
        'rgba(0,0,255,0.3)',
      ],
    },
  ];

  constructor() { }

  public ngOnInit() { }

  public ngOnChanges() {
    this.puttingDataIntoPieChart();
  }

  public puttingDataIntoPieChart() {
    this.redemptionActivities.forEach((activity: RedemptionActivity) => {
      const indexOfRedmptionStatus = this.pieChartLabels.indexOf(
        activity.redemptionStatus
      );
      if (indexOfRedmptionStatus === -1) {
        this.pieChartLabels.push(activity.redemptionStatus);
        this.pieChartData.push(0);
      }
    });

    this.redemptionActivities.forEach((activity: RedemptionActivity) => {
      const indexOfRedmptionStatus = this.pieChartLabels.indexOf(
        activity.redemptionStatus
      );
      if (indexOfRedmptionStatus > -1) {
        this.pieChartData[indexOfRedmptionStatus]++;
      }
    });
  }

  public changeLegendPosition() {
    this.pieChartOptions.legend.position =
      this.pieChartOptions.legend.position === 'left' ? 'top' : 'left';
  }


  public chartClicked({
    event,
    active,
  }: {
    event: MouseEvent;
    active: Array<{}>;
  }): void {
    console.log(event, active);
  }

  public chartHovered({
    event,
    active,
  }: {
    event: MouseEvent;
    active: Array<{}>;
  }): void {
    console.log(event, active);
  }
}
