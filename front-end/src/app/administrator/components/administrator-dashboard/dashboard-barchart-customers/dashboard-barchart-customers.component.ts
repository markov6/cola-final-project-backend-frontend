import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { ChartDataSets, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import * as moment from 'moment';
import { Label } from 'ng2-charts';
import { RedemptionActivity } from 'src/app/common/models/redemption-activity';

@Component({
  selector: 'app-dashboard-barchart-customers',
  templateUrl: './dashboard-barchart-customers.component.html',
  styleUrls: ['./dashboard-barchart-customers.component.scss'],
})
export class DashboardBarchartCustomersComponent implements OnInit, OnChanges {
  public loaded = false;
  @Input() public redemptionActivities: RedemptionActivity[];
  public barChartSuccessfullStatus = [0, 0, 0, 0, 0, 0, 0];
  public barChartInvalidStatus = [0, 0, 0, 0, 0, 0, 0];
  public barChartCancelledStatus = [0, 0, 0, 0, 0, 0, 0];
  public barChartRedemeedStatus = [0, 0, 0, 0, 0, 0, 0];

  // public barChartOptions: ChartOptions = {
  //   responsive: true,
  //   // We use these empty structures as placeholders for dynamic theming.
  //   scales: { xAxes: [{}], yAxes: [{}] },
  //   plugins: {
  //     datalabels: {
  //       anchor: 'end',
  //       align: 'end',
  //     }
  //   }
  // };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];
  public barChartData: ChartDataSets[] = [
    { data: [], label: 'Succesful Attempts This Day' },
    { data: [], label: 'Invalid Attempts This Day' },
    { data: [], label: 'Cancelled Attempts This Day' },
    { data: [], label: 'Redemeed Attempts This Day' },
  ];
  constructor() {}

  public ngOnInit() {
    this.getLastSevenDays();
    this.pushDataIntoLabels();
  }

  public ngOnChanges() {
    this.pushDataIntoLabels();
  }

  public getLastSevenDays() {
    const days = [];
    const daysRequired = 7;

    for (let i = daysRequired; i >= 1; i--) {
      days.push(
        moment()
          .subtract(i, 'days')
          .format('dddd, Do MMMM YYYY')
      );
      this.barChartLabels.push(
        moment()
          .subtract(i, 'days')
          .format('Do MMMM YYYY')
      );
    }
  }

  public pushDataIntoLabels() {
    this.redemptionActivities.forEach((activity: RedemptionActivity) => {
      const activityDate = moment(activity.timestamp).format('Do MMMM YYYY');
      const currentActivity = activity.redemptionStatus;
      if (this.barChartLabels.includes(activityDate)) {
        const indexOfTheDate = this.barChartLabels.indexOf(activityDate);
        if (currentActivity === 'invalid' && indexOfTheDate > -1) {
          this.barChartInvalidStatus[indexOfTheDate]++;
        }
        if (currentActivity === 'successful' && indexOfTheDate > -1) {
          this.barChartSuccessfullStatus[indexOfTheDate]++;
        }
        if (currentActivity === 'redeemed' && indexOfTheDate > -1) {
          this.barChartRedemeedStatus[indexOfTheDate]++;
        }
        if (currentActivity === 'cancelled' && indexOfTheDate > -1) {
          this.barChartCancelledStatus[indexOfTheDate]++;
        }
      }
    });

    this.barChartData[0].data = this.barChartSuccessfullStatus.map(
      item => item
    );
    this.barChartData[1].data = this.barChartInvalidStatus.map(item => item);
    this.barChartData[2].data = this.barChartCancelledStatus.map(item => item);
    this.barChartData[3].data = this.barChartRedemeedStatus.map(item => item);
    this.loaded = true;
  }

  public chartClicked({
    event,
    active,
  }: {
    event: MouseEvent;
    active: Array<{}>;
  }): void {
    console.log(event, active);
  }

  public chartHovered({
    event,
    active,
  }: {
    event: MouseEvent;
    active: Array<{}>;
  }): void {
    console.log(event, active);
  }
}
