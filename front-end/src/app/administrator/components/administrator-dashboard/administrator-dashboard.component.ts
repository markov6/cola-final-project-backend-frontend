import { WeekDay } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import { Subscription } from 'rxjs';
import { RedemptionActivity } from 'src/app/common/models/redemption-activity';
import { AuthService } from 'src/app/core/services/auth.service';
import { RedemptionActivitiesService } from 'src/app/users/services/redemption-activities.service';

@Component({
  selector: 'app-administrator-dashboard',
  templateUrl: './administrator-dashboard.component.html',
  styleUrls: ['./administrator-dashboard.component.scss'],
})
export class AdministratorDashboardComponent implements OnInit {
  public redemptionActivities: RedemptionActivity[] = [];
  public username = '';
  public isAdmin = false;
  public filterUser = '';

  private subscription: Subscription;
  private roleSubscription: Subscription;

  constructor(
    private readonly redemptionActivitiesService: RedemptionActivitiesService,
    private readonly auth: AuthService
  ) {}

  public ngOnInit() {
    this.checkUserROle();
    this.checkUserSub();
    this.getRedemptionActivitiesData();
  }

  public checkUserSub() {
    this.subscription = this.auth.user$.subscribe(username => {
      if (username === null) {
        this.username = '';
        // this.notificator.success(`You have logged out.`);
      } else {
        this.username = username;
      }
    });
  }

  public checkUserROle() {
    this.roleSubscription = this.auth.role$.subscribe(role => {
      if (role === 'admin') {
        this.isAdmin = true;
      } else {
        this.isAdmin = false;
      }
    });
  }

  public getRedemptionActivitiesData() {
    this.redemptionActivitiesService
      .getAllRedemptionActivities()
      .subscribe((data: RedemptionActivity[]) => {
        this.redemptionActivities = data;
      });
  }
}
