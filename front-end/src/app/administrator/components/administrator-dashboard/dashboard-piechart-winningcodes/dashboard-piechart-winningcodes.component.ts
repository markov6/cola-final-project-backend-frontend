import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import { Prize } from 'src/app/common/models/prize';
import { RedemptionActivity } from 'src/app/common/models/redemption-activity';
import { CodeCheckingService } from 'src/app/core/services/code-checking.service';

@Component({
  selector: 'app-dashboard-piechart-winningcodes',
  templateUrl: './dashboard-piechart-winningcodes.component.html',
  styleUrls: ['./dashboard-piechart-winningcodes.component.scss'],
})
export class DashboardPiechartWinningcodesComponent
  implements OnInit, OnChanges {
  public allWinningCodes: Prize[] = [];
  public allRedeemed = 0;
  public allCodes = 0;
  @Input() public redemptionActivities: RedemptionActivity[];

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'left',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    },
  };
  public pieChartLabels: Label[] = [];
  public pieChartData: number[] = [];
  public pieChartPlugins = [pluginDataLabels];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartColors = [
    {
      backgroundColor: [
        'rgba(255,0,0,0.3)',
        'rgba(0,255,0,0.3)',
        'rgba(0,0,255,0.3)',
      ],
    },
  ];

  constructor(public readonly winningCodesService: CodeCheckingService) { }

  public ngOnInit() {
    this.gettingAllWinningCodes();
  }

  public ngOnChanges() {
    this.pushingDataIntoWinningCodeChart();
  }

  public gettingAllWinningCodes() {
    this.winningCodesService.getAllWinningCodes().subscribe((data: Prize[]) => {
      this.allWinningCodes = data;
    });
  }

  public pushingDataIntoWinningCodeChart() {
    this.allWinningCodes.forEach((winningCode: Prize) => {
      let labelOfWinningCode = winningCode.redeemed.toString();
      if (labelOfWinningCode === 'true') {
        labelOfWinningCode = 'Redeemed Codes';
      }
      if (labelOfWinningCode === 'false') {
        labelOfWinningCode = 'Not Redeemed Codes';
      }
      const indexOfWinningCode = this.pieChartLabels.indexOf(
        labelOfWinningCode
      );
      if (indexOfWinningCode === -1) {
        this.pieChartLabels.push(labelOfWinningCode);
        this.pieChartData.push(0);
      }
    });

    this.allWinningCodes.forEach((winningCode: Prize) => {
      let labelOfWinningCode = winningCode.redeemed.toString();
      if (labelOfWinningCode === 'true') {
        labelOfWinningCode = 'Redeemed Codes';
      }
      if (labelOfWinningCode === 'false') {
        labelOfWinningCode = 'Not Redeemed Codes';
      }
      const indexOfWinningCode = this.pieChartLabels.indexOf(
        labelOfWinningCode
      );
      if (indexOfWinningCode > -1) {
        this.pieChartData[indexOfWinningCode]++;
      }
    });
  }

  public changeLegendPosition() {
    this.pieChartOptions.legend.position =
      this.pieChartOptions.legend.position === 'left' ? 'top' : 'left';
  }

  public chartClicked({
    event,
    active,
  }: {
    event: MouseEvent;
    active: Array<{}>;
  }): void {
    console.log(event, active);
  }

  public chartHovered({
    event,
    active,
  }: {
    event: MouseEvent;
    active: Array<{}>;
  }): void {
    console.log(event, active);
  }
}
