import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardPiechartWinningcodesComponent } from './dashboard-piechart-winningcodes.component';

describe('DashboardPiechartWinningcodesComponent', () => {
  let component: DashboardPiechartWinningcodesComponent;
  let fixture: ComponentFixture<DashboardPiechartWinningcodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardPiechartWinningcodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardPiechartWinningcodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
