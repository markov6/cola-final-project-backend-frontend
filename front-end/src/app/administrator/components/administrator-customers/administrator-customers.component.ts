import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Customer } from 'src/app/common/models/customer';
import { Outlet } from 'src/app/common/models/outlet';
import { AuthService } from 'src/app/core/services/auth.service';
import { CustomerService } from 'src/app/core/services/customer.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { OutletService } from 'src/app/core/services/outlet.service';

@Component({
  selector: 'app-administrator-customers',
  templateUrl: './administrator-customers.component.html',
  styleUrls: ['./administrator-customers.component.scss'],
})
export class AdministratorCustomersComponent implements OnInit, OnDestroy {
  // this is build in pagination and sorting for MatTableDataSource
  @ViewChild(MatPaginator) public paginator: MatPaginator;
  @ViewChild(MatSort) public sort: MatSort;

  // this is the data used in the table
  public dataSource: MatTableDataSource<any>;

  public columnsToDisplay = ['id', 'name', 'delete', 'edit'];

  public customers: Customer[] = [];
  public isAdmin = false;
  public username = '';

  private subscription: Subscription;
  private roleSubscription: Subscription;
  private searchSubscription: Subscription;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly customerService: CustomerService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly auth: AuthService
  ) {}

  public ngOnInit() {
    this.checkAuthUser();
    this.checkRoleSubscription();
    this.putDetailsIntoMatTable();
  }

  public ngOnDestroy(): void {
    this.roleSubscription.unsubscribe();
    this.subscription.unsubscribe();
  }

  public checkRoleSubscription() {
    this.roleSubscription = this.auth.role$.subscribe(role => {
      if (role === 'admin') {
        this.isAdmin = true;
      } else {
        this.isAdmin = false;
      }
    });
  }

  public checkAuthUser() {
    this.subscription = this.auth.user$.subscribe(username => {
      if (username === null) {
        this.username = '';
        // this.notificator.success(`You have logged out.`);
      } else {
        this.username = username;
      }
    });
  }

  public putDetailsIntoMatTable() {
    this.customerService.getAllCustomers().subscribe((data: Customer[]) => {
      this.customers = data;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  public deleteCustomer(id: string): void {
    this.customerService.deleteCustomer(id).subscribe(
      () => {
        this.ngOnInit();
        this.notificator.success('Customer was successfully deleted!');
      },
      err => {
        this.notificator.error(
          'There was an error while deleting the Customer!'
        );
      }
    );
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public redirectToEditCustomer(id: string) {
    this.router.navigate([`administrator/customers/${id}`]);
  }
}
