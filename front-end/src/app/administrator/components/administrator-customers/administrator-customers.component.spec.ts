import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministratorCustomersComponent } from './administrator-customers.component';

describe('AdministratorCustomersComponent', () => {
  let component: AdministratorCustomersComponent;
  let fixture: ComponentFixture<AdministratorCustomersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdministratorCustomersComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministratorCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
