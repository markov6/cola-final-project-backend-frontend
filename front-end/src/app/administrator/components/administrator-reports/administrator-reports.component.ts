import { DatePipe } from '@angular/common';
import {
  Component,
  OnInit,
  ViewChild,
  OnChanges,
  OnDestroy,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { RedemptionActivity } from 'src/app/common/models/redemption-activity';
import { AuthService } from 'src/app/core/services/auth.service';
import { RedemptionActivitiesService } from 'src/app/users/services/redemption-activities.service';

@Component({
  selector: 'app-administrator-reports',
  templateUrl: './administrator-reports.component.html',
  styleUrls: ['./administrator-reports.component.scss'],
})
export class AdministratorReportsComponent implements OnInit, OnDestroy {
  // this is build in pagination and sorting for MatTableDataSource
  @ViewChild(MatPaginator) public paginator: MatPaginator;
  @ViewChild(MatSort) public sort: MatSort;

  // this is the data used in the table
  public dataSource: MatTableDataSource<any>;
  public secondDataSource: MatTableDataSource<any>;
  public pipe: DatePipe;

  public filterString = '';

  public columnsToDisplay = [
    'id',
    'timestamp',
    'code',
    'outlet',
    'customer',
    'redemptionStatus',
    'prizeCode',
    'username',
  ];

  public filterForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
  });

  public redemptionActivities: RedemptionActivity[] = [];
  public username = '';
  public isAdmin = false;
  public filterUser = '';

  private subscription: Subscription;
  private roleSubscription: Subscription;

  constructor(
    private readonly redemptionActivitiesService: RedemptionActivitiesService,
    private readonly auth: AuthService,
  ) { }

  public ngOnInit() {
    this.checkUserRole();
    this.checkUserSub();
    this.redemptionActivitiesDataAssignment();
  }

  public checkUserSub() {
    this.subscription = this.auth.user$.subscribe(username => {
      if (username === null) {
        this.username = '';
        // this.notificator.success(`You have logged out.`);
      } else {
        this.username = username;
      }
    });
  }

  public checkUserRole() {
    this.roleSubscription = this.auth.role$.subscribe(role => {
      if (role === 'admin') {
        this.isAdmin = true;
      } else {
        this.isAdmin = false;
      }
    });
  }

  public redemptionActivitiesDataAssignment() {
    this.redemptionActivitiesService
      .getAllRedemptionActivities()
      .subscribe((data: RedemptionActivity[]) => {
        this.redemptionActivities = data;

        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.getDateRange();
      });
  }

  get fromDate() {
    return this.filterForm.get('fromDate').value;
  }

  get toDate() {
    return this.filterForm.get('toDate').value;
  }

  public ngOnDestroy(): void {
    this.roleSubscription.unsubscribe();
    this.subscription.unsubscribe();
  }

  public applyFilter(filterValue: string) {
    this.filterString = filterValue.trim().toLowerCase();

    this.dataSource.filter = filterValue;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public getDateRange() {
    this.pipe = new DatePipe('en');
    this.dataSource.filterPredicate = (data: RedemptionActivity, filter) => {
      let result = true;
      const barCode = data.prizeCode.toLowerCase().indexOf(this.filterString.toLowerCase()) >=
        0;
      const customers =
        data.customer.toLowerCase().indexOf(this.filterString.toLowerCase()) >=
        0;
      const outlets =
        data.outlet.toLowerCase().indexOf(this.filterString.toLowerCase()) >= 0;
      const redemptionStatus =
        data.redemptionStatus
          .toLowerCase()
          .indexOf(this.filterString.toLowerCase()) >= 0;
      const cashierName =
        data.username.toLowerCase().indexOf(this.filterString.toLowerCase()) >=
        0;

      if (this.fromDate && this.toDate) {
        const actualDate = Date.parse((data as any).timestamp);
        const fromDate = Date.parse(
          moment(this.fromDate, 'DD/MM/YYYY', true).format(),
        );
        const toDate = Date.parse(
          moment(this.toDate, 'DD/MM/YYYY', true).format(),
        );

        result = actualDate >= fromDate && actualDate <= toDate;
      }

      if (!result) {
        return false;
      }

      if (this.filterString) {
        result =
          (result && customers) ||
          (result && outlets) ||
          (result && redemptionStatus) ||
          (result && cashierName) ||
          (result && barCode);
      }

      return result;
    };
  }

  public applyDateFilter() {
    this.dataSource.filter = '' + Math.random();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
