import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministratorUserActivitiesComponent } from './administrator-user-activities.component';

describe('AdministratorUserActivitiesComponent', () => {
  let component: AdministratorUserActivitiesComponent;
  let fixture: ComponentFixture<AdministratorUserActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdministratorUserActivitiesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministratorUserActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
