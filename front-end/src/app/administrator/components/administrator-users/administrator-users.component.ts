import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/common/models/user';
import { AuthService } from 'src/app/core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { UsersService } from 'src/app/core/services/users.service';

@Component({
  selector: 'app-administrator-users',
  templateUrl: './administrator-users.component.html',
  styleUrls: ['./administrator-users.component.scss'],
})
export class AdministratorUsersComponent implements OnInit, OnDestroy {
  // this is build in pagination and sorting for MatTableDataSource
  @ViewChild(MatPaginator) public paginator: MatPaginator;
  @ViewChild(MatSort) public sort: MatSort;

  // this is the data used in the table
  public dataSource: MatTableDataSource<any>;

  public columnsToDisplay = [
    'id',
    'name',
    'email',
    'createdOn',
    'phone',
    'delete',
    'edit',
  ];


  public users: User[] = [];
  public username = '';
  public isAdmin = false;
  public filterUser = '';

  private subscription: Subscription;
  private roleSubscription: Subscription;

  constructor(
    private readonly userService: UsersService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly auth: AuthService
  ) { }

  public ngOnInit() {
    this.checkUserRole();
    this.checkUserSub();
    this.getAllUsersData();
  }

  public checkUserSub() {
    this.subscription = this.auth.user$.subscribe(username => {
      if (username === null) {
        this.username = '';
        // this.notificator.success(`You have logged out.`);
      } else {
        this.username = username;
      }
    });
  }

  public checkUserRole() {
    this.roleSubscription = this.auth.role$.subscribe(role => {
      if (role === 'admin') {
        this.isAdmin = true;
      } else {
        this.isAdmin = false;
      }
    });
  }

  public getAllUsersData() {
    this.userService.getAllUsers().subscribe((data: User[]) => {
      this.users = data;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  public ngOnDestroy(): void {
    this.roleSubscription.unsubscribe();
    this.subscription.unsubscribe();
  }

  public deleteUser(id: string): void {
    this.userService.deleteUser(id).subscribe(
      () => {
        this.ngOnInit();
        this.notificator.success('User was successfully deleted!');
      },
      err => {
        this.notificator.error('There was an error while deleting the User!');
      }
    );
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public redirectToEditUser(id: string) {
    this.router.navigate([`administrator/users/${id}`]);
  }
}
