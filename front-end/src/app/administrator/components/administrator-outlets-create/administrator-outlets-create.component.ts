import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CreateOutlet } from 'src/app/common/models/create-outlet';
import { Customer } from 'src/app/common/models/customer';
import { Outlet } from 'src/app/common/models/outlet';
import { CustomerService } from 'src/app/core/services/customer.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { OutletService } from 'src/app/core/services/outlet.service';

@Component({
  selector: 'app-administrator-outlets-create',
  templateUrl: './administrator-outlets-create.component.html',
  styleUrls: ['./administrator-outlets-create.component.scss'],
})
export class AdministratorOutletsCreateComponent implements OnInit {
  public createOutletForm: FormGroup;
  public brands: Customer[] = [];
  public isComponentInCreateState = true;
  public outlet: Outlet;

  constructor(
    private readonly outletService: OutletService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly customerService: CustomerService,
    public readonly activatedRoute: ActivatedRoute
  ) { }

  public ngOnInit() {
    this.getAllCustomers();
    this.createFormForTheOutlet();
    this.checkComponentStateCreateOrEdit();
  }

  public checkComponentStateCreateOrEdit() {
    const id: string = this.activatedRoute.snapshot.paramMap.get('id');
    if (id === 'outletscreate') {
      this.isComponentInCreateState = true;
    } else {
      this.isComponentInCreateState = false;
      this.putOutletDetailsIntoUpdateForm();
    }
  }

  public getAllCustomers() {
    this.customerService.getAllCustomers().subscribe((data: Customer[]) => {
      this.brands = data;
    });
  }

  public createFormForTheOutlet() {
    this.createOutletForm = this.formBuilder.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(30),
        ],
      ],
      brand: [
        '',
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(30),
        ],
      ],
    });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.createOutletForm.controls[controlName].hasError(errorName);
  };

  public createOutlet(): void {
    const createTheOutlet: CreateOutlet = this.createOutletForm.value;

    this.outletService.createOutlet(createTheOutlet).subscribe(
      () => {
        this.notificator.success(`Succesfully create the outlet!`);
        this.router.navigate([`administrator/outlets`]);
      },
      () => {
        this.notificator.error(`Outlet creation failed!`);
      }
    );
  }

  public editOutlet(): void {
    const editTheOutlet: CreateOutlet = this.createOutletForm.value;
    const id: string = this.activatedRoute.snapshot.paramMap.get('id');
    this.outletService.updateOutlet(id, editTheOutlet).subscribe(
      () => {
        this.notificator.success(`Succesfully updated the outlet!`);
        this.router.navigate([`administrator/outlets`]);
      },
      () => {
        this.notificator.error(`Outlet Update Failed`);
      }
    );
  }

  public putOutletDetailsIntoUpdateForm() {
    const id: string = this.activatedRoute.snapshot.paramMap.get('id');
    this.outletService.getOutlet(id).subscribe((data: Outlet) => {
      this.outlet = data;
      this.createOutletForm.patchValue({
        name: this.outlet.name,
        brand: this.outlet.newBrand,
      });
    });
  }
}
