import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministratorUsersCreateComponent } from './administrator-users-create.component';

describe('AdministratorUsersCreateComponent', () => {
  let component: AdministratorUsersCreateComponent;
  let fixture: ComponentFixture<AdministratorUsersCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdministratorUsersCreateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministratorUsersCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();

    fixture.nativeElement.querySelector("")
  });

});
