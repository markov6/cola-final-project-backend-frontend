import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CreateUser } from 'src/app/common/models/create-user';
import { Customer } from 'src/app/common/models/customer';
import { Outlet } from 'src/app/common/models/outlet';
import { User } from 'src/app/common/models/user';
import { CustomerService } from 'src/app/core/services/customer.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { OutletService } from 'src/app/core/services/outlet.service';
import { UsersService } from 'src/app/core/services/users.service';

@Component({
  selector: 'app-administrator-users-create',
  templateUrl: './administrator-users-create.component.html',
  styleUrls: ['./administrator-users-create.component.scss'],
})
export class AdministratorUsersCreateComponent implements OnInit {
  public createUserForm: FormGroup;
  public outlets: Outlet[] = [];
  public customers: Customer[] = [];
  public selectedCustomer: Customer;
  public customer: string;
  public outlet: string;
  public selectedOutlets: Outlet[] = [];
  public selectedValue: string;
  public user: User;
  public isComponentInCreateState = true;

  constructor(
    private readonly userService: UsersService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly outletService: OutletService,
    private readonly customerSerivce: CustomerService,
    public routeForTheId: ActivatedRoute
  ) {}

  public ngOnInit() {
    this.createEditForm();
    this.getAllCustomers();
    this.getAllOutlets();
    this.checkComponentState();
  }

  public checkComponentState() {
    const id: string = this.routeForTheId.snapshot.paramMap.get('id');
    if (id === 'userscreate') {
      this.isComponentInCreateState = true;
    } else {
      this.isComponentInCreateState = false;
      this.putUserDetailsIntoUpdateForm();
    }
  }

  public createEditForm() {
    this.createUserForm = this.formBuilder.group(
      {
        name: ['', [Validators.required, Validators.minLength(6)]],
        password: [
          '',
          [
            Validators.required,
            Validators.pattern(
              /(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/
            ),
          ],
        ],
        confirmPassword: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        phone: [
          '',
          [
            Validators.required,
            Validators.pattern(
              /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
            ),
          ],
        ],
        outlet: ['', [Validators.required]],
      },
      { validator: this.checkPasswords }
    );
  }

  public getAllOutlets() {
    this.outletService.getAllOutlets().subscribe((data: []) => {
      this.outlets = data;
    });
  }

  public getAllCustomers() {
    this.customerSerivce.getAllCustomers().subscribe((data: Customer[]) => {
      this.customers = data;
    });
  }

  public getSelectedCustomer(selectedValue) {
    this.selectedOutlets = [];
    this.customerSerivce
      .getCustomer(selectedValue)
      .subscribe((data: Customer) => {
        this.selectedCustomer = data;
        this.outlets.forEach(outlet => {
          const currentOutlet = outlet;
          if (this.selectedCustomer.name === outlet.newBrand) {
            this.selectedOutlets.push(currentOutlet);
          }
        });
      });
  }

  public putUserDetailsIntoUpdateForm() {
    const id: string = this.routeForTheId.snapshot.paramMap.get('id');
    this.userService.getUser(id).subscribe((data: User) => {
      this.user = data;
      this.createUserForm.patchValue({
        name: this.user.name,
        email: this.user.email,
        phone: this.user.phone,
        outlet: this.user.outlet,
      });
    });
  }

  public createUser(): void {
    const createTheUser: CreateUser = this.createUserForm.value;
    this.userService.createUser(createTheUser).subscribe(
      () => {
        this.notificator.success(`Succesfully created the user!`);
        this.router.navigate([`administrator/users`]);
      },
      () => {
        this.notificator.error(`User creation failed!`);
      }
    );
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.createUserForm.controls[controlName].hasError(errorName);
  };

  public editUser(): void {
    const editTheUser: CreateUser = this.createUserForm.value;
    const id: string = this.routeForTheId.snapshot.paramMap.get('id');
    this.userService.updateUser(id, editTheUser).subscribe(
      () => {
        this.notificator.success(`Succesfully updated the user!`);
        this.router.navigate([`administrator/users`]);
      },
      () => {
        this.notificator.error(`User Update Failed`);
      }
    );
  }

  public checkPasswords(group: FormGroup) {
    const password = group.controls.password.value;
    const confirmPassword = group.controls.confirmPassword.value;

    return password === confirmPassword ? null : { notSame: true };
  }
}
