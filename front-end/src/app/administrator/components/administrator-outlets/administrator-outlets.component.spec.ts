import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AdministratorOutletsComponent } from './administrator-outlets.component';
import { Outlet } from 'src/app/common/models/outlet';
import { AdministratorRoutingModule } from '../../administrator-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder, FormsModule } from '@angular/forms';
import { OutletService } from 'src/app/core/services/outlet.service';
import { CustomerService } from 'src/app/core/services/customer.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { AdministratorHomeComponent } from '../administrator-home/administrator-home.component';
import { AdministratorModule } from '../../administrator.module';
import { CoreModule } from 'src/app/core/core.module';
import { AdministratorOutletsCreateComponent } from '../administrator-outlets-create/administrator-outlets-create.component';
import { of } from 'rxjs';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { User } from 'src/app/common/models/user';

fdescribe('AdministratorOutletsComponent', () => {

  let component: AdministratorOutletsComponent;
  let fixture;
  let isMockAdmin = true;

  const mockOutlets: Outlet[] = [
    {
      id: '1',
      name: 'newName',
      newBrand: 'brand'
    },
    {
      id: '2',
      name: 'nextOutlet',
      newBrand: 'secondBrand'
    }
  ]

  const outletService = jasmine.createSpyObj('OutletService', ['getAllOutlets', 'getOutlet', 'createOutlet', 'deleteOutlet', 'updateOutlet']);
  const notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);
  const router = jasmine.createSpyObj('Router', ['navigate']);
  const authService = jasmine.createSpyObj('AuthService', ['role$', 'user$']);

  outletService.getAllOutlets.and.returnValue(mockOutlets);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
      ],
      imports: [
        AdministratorModule,
        SharedModule,
        CoreModule,
        RouterTestingModule,
        FormsModule,
        NoopAnimationsModule
      ],
      providers: [
        FormBuilder,
        {
          provide: OutletService,
          useValue: outletService,
        },
        {
          provide: AuthService,
          useValue: authService
        },
        {
          provide: NotificatorService,
          useValue: notificator,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
  }));

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });

  it('should create the component', () => {
    fixture = TestBed.createComponent(AdministratorOutletsCreateComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  // it('ngOnInit should put correct data into outlets', async () => {
  //   fixture = TestBed.createComponent(AdministratorOutletsComponent);
  //   component = fixture.componentInstance;
  //   component.ngOnInit();


  //   outletService.getAllOutlets.and.returnValue(of(mockOutlets));

  //   await fixture.detectChanges();
  //   expect(component.outlets).toBe(mockOutlets);

  // })


});
