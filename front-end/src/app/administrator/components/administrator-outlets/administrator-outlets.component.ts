import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Outlet } from 'src/app/common/models/outlet';
import { AuthService } from 'src/app/core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { OutletService } from 'src/app/core/services/outlet.service';

@Component({
  selector: 'app-administrator-outlets',
  templateUrl: './administrator-outlets.component.html',
  styleUrls: ['./administrator-outlets.component.scss'],
})
export class AdministratorOutletsComponent implements OnInit, OnDestroy {
  // this is build in pagination and sorting for MatTableDataSource
  @ViewChild(MatPaginator) public paginator: MatPaginator;
  @ViewChild(MatSort) public sort: MatSort;

  // this is the data used in the table
  public dataSource: MatTableDataSource<any>;
  public columnsToDisplay = ['id', 'name', 'delete', 'edit'];
  public outlets: Outlet[] = [];
  public isAdmin = false;
  public username = '';
  private subscription: Subscription;
  private roleSubscription: Subscription;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly outletService: OutletService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly auth: AuthService
  ) { }

  public ngOnInit() {
    this.getUserRole();
    this.getUserSub();
    this.getAllOutlets();
  }

  public getUserRole() {
    this.roleSubscription = this.auth.role$.subscribe(role => {
      if (role === 'admin') {
        this.isAdmin = true;
      } else {
        this.isAdmin = false;
      }
    });
  }

  public getUserSub() {
    this.subscription = this.auth.user$.subscribe(username => {
      if (username === null) {
        this.username = '';
        // this.notificator.success(`You have logged out.`);
      } else {
        this.username = username;
      }
    });
  }

  public getAllOutlets() {
    this.outletService.getAllOutlets().subscribe((data: Outlet[]) => {
      this.outlets = data;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  public ngOnDestroy(): void {
    this.roleSubscription.unsubscribe();
    this.subscription.unsubscribe();
  }

  public deleteOutlet(id: string): void {
    this.outletService.deleteOutlet(id).subscribe(
      () => {
        this.ngOnInit();
        this.notificator.success('Outlet was successfully deleted!');
      },
      err => {
        this.notificator.error('There was an error while deleting the outlet!');
      }
    );
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public redirectToEditOutlet(id: string) {
    this.router.navigate([`administrator/outlets/${id}`]);
  }
}
