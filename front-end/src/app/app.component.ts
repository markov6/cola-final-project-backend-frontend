import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SwUpdate } from '@angular/service-worker';
import { of, Subscription } from 'rxjs';
import { AuthService } from './core/services/auth.service';
import { NotificatorService } from './core/services/notificator.service';
import { UsersService } from './core/services/users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  public username = '';
  public isLogged = false;
  public isAdmin = false;
  public userId = '';
  private subscription: Subscription;
  private roleSubscription: Subscription;
  private userIdSubscription: Subscription;

  constructor(
    private readonly notificator: NotificatorService,
    private readonly auth: AuthService,
    private readonly router: Router,
    private readonly swUpdate: SwUpdate
  ) { }

  public ngOnInit() {
    this.subscription = this.auth.user$.subscribe(username => {
      if (username === null) {
        this.username = '';
        this.isLogged = false;
      } else {
        this.username = username;
        this.isLogged = true;
      }
    });

    this.userIdSubscription = this.auth.userId$.subscribe(userId => {
      if (userId === null) {
        this.userId = '';
        this.isLogged = false;
      } else {
        this.userId = userId;
        this.isLogged = true;
      }
    });

    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        if (confirm('New version available. Load New Version?')) {
          window.location.reload();
        }
      });
    }

    this.roleSubscription = this.auth.role$.subscribe(role => {
      if (role === 'admin') {
        this.isAdmin = true;
      } else {
        this.isAdmin = false;
      }
    });
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
    this.roleSubscription.unsubscribe();
  }

  public logout() {
    this.auth.logout();
    // this.router.navigateByUrl('/home').then(
    //   data => {console.log(data)},
    //   err => console.log(err));
    this.notificator.success(`You have logged out.`);
  }
}
