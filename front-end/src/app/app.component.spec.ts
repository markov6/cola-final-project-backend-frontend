import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { UsersModule } from './users/users.module';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ThemePickerComponent } from './components/theme-picker/theme-picker.component';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { SwUpdate, ServiceWorkerModule } from '@angular/service-worker';
import { environment } from 'src/environments/environment';
import { NotificatorService } from './core/services/notificator.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './core/services/auth.service';
import { of } from 'rxjs';

describe('AppComponent', () => {
  let fixture;

  let notificator = jasmine.createSpyObj('NotificatorService', [
    'success',
    'error',
  ]);
  let auth = jasmine.createSpyObj('AuthService', [
    'login',
    'loginAdmin',
    'logout',
    'register',
  ]);
  let swUpdate = jasmine.createSpyObj('SwUpdate', ['available']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        SharedModule,
        AppRoutingModule,
        UsersModule,
      ],
      declarations: [
        AppComponent,
        HomeComponent,
        NavbarComponent,
        NotFoundComponent,
        ThemePickerComponent,
      ],
      providers: [
        {
          provide: NotificatorService,
          useValue: notificator,
        },
        {
          provide: AuthService,
          useValue: auth,
        },
        {
          provide: SwUpdate,
          useValue: swUpdate,
        },
      ],
    });
  }));

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });

  it('should create the app', () => {
    fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  // // test ngOnInit
  // it(`should initialize the correct logged user data`, async () => {
  //   // create observable for user
  //   auth.user$ = of('test');
  //   auth.role$ = of('admin');
  //   fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;

  //   // await the component initialization
  //   await fixture.detectChanges();
  //   expect(app.username).toBe('test');
  //   expect(app.isLogged).toBe(true);
  //   expect(app.isAdmin).toBe(true);
  // })

  // it(`should initialize correctly witn no logged user`, async () => {
  //   // create observable for user
  //   auth.user$ = of(null);
  //   fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;

  //   // await the component initialization
  //   await fixture.detectChanges();
  //   expect(app.username).toBe('');
  //   expect(app.isLogged).toBe(false);
  // })
});
