export interface CreateOutlet {
  name: string;
  brand: string;
}
