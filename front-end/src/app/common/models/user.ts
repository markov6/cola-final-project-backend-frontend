export interface User {
  id: string;
  name: string;
  email: string;
  createdOn: string;
  version: number;
  phone: string;
  outlet: string;
}
