export interface InitialCodeLog {
  code: string;
  winning: boolean;
}
