export interface Prize {
  code: string;
  prize: string;
  redeemed: boolean;
}
