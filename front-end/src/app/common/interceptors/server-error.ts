import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AuthService } from '../../core/services/auth.service';
import { NotificatorService } from '../../core/services/notificator.service';
import { StorageService } from '../../core/services/storage.service';

@Injectable()
export class ServerErrorInterceptor implements HttpInterceptor {
  public constructor(
    private readonly router: Router,
    private readonly notificator: NotificatorService,
    private readonly storage: StorageService,
    private readonly authService: AuthService
  ) {}
  public intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: any) => {
        if (error.status === 401) {
          this.authService.logout();
          this.notificator.error(
            'You should be logged-in in order to do this!'
          );
          this.router.navigate(['home']);
        } else if (error.status === 403) {
          // this.router.navigate(['/home']);
          // this.storage.removeItem('username');
          // this.storage.removeItem('token');
          // this.storage.removeItem('id');
          this.notificator.error('You are not authorized to do this!');
        } else if (error.status === 404) {
          this.router.navigate(['/not-found']);
          this.notificator.error('Resource not found!');
        } else if (error.status >= 500) {
          this.router.navigate(['/server-error']);
          this.notificator.error('Oops.. something went wrong.. :(');
        }

        return throwError(error);
      })
    );
  }
}
