import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Email } from 'src/app/common/models/email';
import { EmailService } from 'src/app/core/services/email.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-helpdesk',
  templateUrl: './helpdesk.component.html',
  styleUrls: ['./helpdesk.component.scss'],
})
export class HelpdeskComponent implements OnInit {
  public emailForm: FormGroup;

  constructor(
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly emailService: EmailService
  ) {}

  public ngOnInit() {
    this.emailForm = this.formBuilder.group({
      sender: ['', [Validators.required, Validators.email]],
      content: ['', [Validators.maxLength(1000), Validators.required]],
    });
  }

  public get sender() {
    return this.emailForm.get('sender');
  }

  public get content() {
    return this.emailForm.get('content');
  }

  public sendEmail(sender: string, message: string) {
    const email: Email = {
      sender,
      message,
      subject: `Helpdesk request!`,
    };
    this.emailService.sendEmail(email).subscribe(
      success => {
        this.notificator.success('Email wass succesfully sent!'),
          this.emailForm.reset();
      },
      error => {
        this.notificator.error(
          `Couldn't send email! Please call +359889112 for help!`
        );
      }
    );
  }
}
