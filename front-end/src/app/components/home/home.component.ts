import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  public username = '';
  public isLogged = false;
  public isAdmin = false;
  public userId = '';
  public role = '';
  private userSubscription: Subscription;
  private roleSubscription: Subscription;
  private idSubscription: Subscription;

  constructor(
    private readonly router: Router,
    private readonly authService: AuthService
  ) {}

  public ngOnInit() {
    this.userSubscription = this.authService.user$.subscribe(username => {
      if (username === null) {
        this.username = '';
        this.isLogged = false;
      } else {
        this.username = username;
        this.isLogged = true;
      }
    });

    this.idSubscription = this.authService.userId$.subscribe(userId => {
      if (userId === null) {
        this.userId = '';
        this.isLogged = false;
      } else {
        this.userId = userId;
        this.isLogged = true;
      }
    });

    this.roleSubscription = this.authService.role$.subscribe(role => {
      if (role === null) {
        this.role = '';
        this.isLogged = false;
      } else {
        this.role = role;
        this.isLogged = true;
      }
    });

    if (this.userId !== '' && this.role === 'user') {
      console.log(`home component ${this.userId} and role is ${this.role}`);
      this.router.navigate([`users/profile/${this.userId}`]);
    }
    if (this.userId !== '' && this.role === 'admin') {
      this.router.navigate(['administrator/home']);
    }
  }

  public ngOnDestroy() {
    this.roleSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
    this.idSubscription.unsubscribe();
  }
}
