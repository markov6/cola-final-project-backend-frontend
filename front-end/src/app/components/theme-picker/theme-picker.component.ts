import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { StorageService } from 'src/app/core/services/storage.service';
import { StyleManagerService } from 'src/app/core/services/style-manager.service';

@Component({
  selector: 'app-theme-picker',
  templateUrl: './theme-picker.component.html',
  styleUrls: ['./theme-picker.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ThemePickerComponent implements OnInit {
  public themes: CustomTheme[] = [
    {
      primary: '#ffeb3b',
      accent: '#448aff',
      name: 'yellow-blue',
      isDark: false,
    },
    {
      primary: '#3F51B5',
      accent: '#E91E63',
      name: 'indigo-pink',
      isDark: false,
      isDefault: true,
    },
    {
      primary: '#f44336',
      accent: '#ff4081',
      name: 'red-pink-dark',
      isDark: true,
    },
    {
      primary: '#f44336',
      accent: '#ff4081',
      name: 'red-pink-light',
      isDark: false,
    },
  ];

  constructor(
    public styleManager: StyleManagerService,
    public storage: StorageService
  ) {}

  public ngOnInit() {
    const selectedTheme = this.storage.getItem('theme');
    if (selectedTheme) {
      this.installTheme(selectedTheme);
    } else {
      this.installTheme('red-pink-light');
    }
  }

  public installTheme(themeName: string) {
    const theme = this.themes.find(
      currentTheme => currentTheme.name === themeName
    );
    if (!theme) {
      return;
    }

    if (theme.isDefault) {
      this.styleManager.removeStyle('theme');
    } else {
      this.styleManager.setStyle('theme', `/assets/${theme.name}.css`);
      this.storage.setItem('theme', theme.name);
    }
  }
}

export interface CustomTheme {
  name: string;
  accent: string;
  primary: string;
  isDark?: boolean;
  isDefault?: boolean;
}
